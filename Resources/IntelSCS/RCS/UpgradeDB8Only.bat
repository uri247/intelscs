@echo off
REM ---------------------------------------------------------------------------
REM Copyright (C) 2007-2013, Intel Corporation. All rights reserved.
REM
REM 	UpgradeDB8Only.bat
REM
REM The installer (IntelSCSInstaller.exe) includes options to upgrade
REM the database used by the RCS in database mode.
REM Alternatively, you can use UpgradeDB8Only.bat to upgrade the database.
REM
REM UpgradeDB8Only.bat upgrades the database from version 8.0 to version 8.2 only.
REM No other versions of the database can be upgraded using this script.
REM DO NOT USE THIS FILE IF YOU ARE UPGRADING FROM VERSION 8.1.
REM (Database version 8.1 does not need to be upgraded to version 8.2).
REM   
REM This batch script is used to ensure that a VBScript is run from the cscript engine.
REM The VBScript output is redirected to a file for logging and debugging 
REM purposes. In a production environment redirection should be either removed, 
REM or another mechanism added to prevent the log from filling the host drive.
REM
REM Syntax:
REM 	UpgradeDB8Only.bat <SQL server address> <database name> [ <SQL admin user> <SQL admin password> ]
REM
REM Example:
REM 	UpgradeDB8Only.bat SQL2008.intel.com\SQL10 IntelSCS8 sa admin1!
REM
REM Note:
REM      If the SQL admin user parameters are not supplied, Windows authentication is used.
REM      For exit codes, refer to the VBScript (DBOperationsvbs).
REM ---------------------------------------------------------------------------

cscript.exe //Nologo //I  .\DBOperations.vbs /sqlserver:%1 /dbname:%2 /sqluser:%3 /sqlpass:%4 /scripts:. /operation:UPGRADE /preinstalled:FALSE /dbversion:'8.2.0'

