------------------------------------------------------------------
-- Copyright (C) 2003-2013, Intel Corporation. All rights reserved.
------------------------------------------------------------------
use $(db_name)

--alter AMTs tables
ALTER TABLE [dbo].[amt]
	ADD [host_mismatch] [int],
		[mismatch_target_fqdn] [nvarchar](256)

--change version
UPDATE [global_settings] 
SET [configuration_value] = $(scs_version)
WHERE [configuration_name] = 'scs_version'
