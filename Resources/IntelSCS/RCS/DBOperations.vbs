' ---------------------------------------------------------------------------
' Copyright (C) Intel Corporation, 2007 - 2013. All rights reserved.
' ---------------------------------------------------------------------------
'
' Intel� Setup and Configuration Software (Intel� SCS) for Intel� Active Management Technology (Intel� AMT)
'
' This VBScript is a standalone option that can be used to uninstall or upgrade
' the database used by the RCS in database mode.
'    To uninstall the database, run the script using the UninstallDB.bat file.
'    To upgrade the database, run the script using the UpgradeDB8Only.bat file.
' If the script fails, an error message might be echoed.  
' 
' Note: This script includes installation subscripts that are not supported.
'       Use this file only with the UninstallDB.bat or UpgradeDB8Only.bat files.
'       Use the upgrade option only if you are upgrading from version 8.0.
'       DO NOT USE THE UPGRADE OPTION IF YOU ARE UPGRADING FROM VERSION 8.1.
'       (Database version 8.1 does not need to be upgraded to version 8.2). 
'
' Exit Codes
'
' 1: Could not connect to the database - problem with database connection parameters
' 2: Reading SQL Script failed
' 3: Writing to registry failed
' 6: SQL create databse script execution failed
' 7: SQL create tables script execution failed
' 8: SQL fill values script execution failed
' 9: SQL drop databse script execution failed
Dim CN,  objCmd, WshShell

Set objArgs = WScript.Arguments
sqlServer       = objArgs.Named.Item("sqlserver")
dbName          = objArgs.Named.Item("dbname")
user            = objArgs.Named.Item("sqluser")
pass            = objArgs.Named.Item("sqlpass")
scripts         = objArgs.Named.Item("scripts")
operation       = objArgs.Named.Item("operation")
preinstalled = objArgs.Named.Item("preinstalled")
dbVer = objArgs.Named.Item("dbversion")

'Set authentication type for SQL connection string
If user = "" Then
        sqlAuth = ";Trusted_Connection=yes"
        trustedConn = 1
Else
        sqlAuth = ";User Id = " & user & ";Password = " & pass
        trustedConn = 0
End If

if dbName = "" Then
                Wscript.Echo "Error: DB Name is missing."
                Wscript.Quit(1)
                End If

Const ForReading = 1, ForWriting = 2, ForAppending = 8
Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

On Error Resume Next

Set CN = CreateObject("ADODB.Connection")
Set objCmd = CreateObject("ADODB.Command")
Set WshShell = WScript.CreateObject("WScript.Shell")

' Create Database
CN.ConnectionString = "Provider=SQLOLEDB.1;" & "Server=" & sqlServer & ";DataBase=master"  & sqlAuth
'wscript.echo CN.ConnectionString
CN.Open
If Err.Number <> 0 Then 
        Wscript.Echo "Error: " & Err.Number & "; " & Err.Description
        Wscript.Quit(1)
End If

If UCase(operation) = "DROPDB" Then
        Call ExecuteSqlFile (scripts & "\dropdb.sql",CN,dbName,dbVer,0)
        If Err.Number <> 0 Then 
                Wscript.Echo "Error: " & Err.Number & "; " & Err.Description
                Wscript.Quit(9)
        End If
        'Call DeleteSCSRegistry() For the time being Do Not delete
Elseif UCase(operation) = "UPGRADE" Then
		Call ExecuteSqlFile (scripts & "\upgradedb8only.sql",CN,dbName,dbVer,1)
        If Err.Number <> 0 Then 
                Wscript.Echo "Error: " & Err.Number & "; " & Err.Description
                Wscript.Quit(9)
        End If
ELSE 
        If UCase(preinstalled) = "FALSE" Then
                Call ExecuteSqlFile (scripts & "\builddb.sql",CN,dbName,dbVer,0)
                If Err.Number <> 0 Then 
                        Wscript.Echo "Error: " & Err.Number & "; " & Err.Description
                        Wscript.Quit(6)
                End If
                CN.Close
                CN.ConnectionString = "Provider=SQLOLEDB.1;" & "Server=" & sqlServer & ";DataBase=" & dbName & sqlAuth
                CN.Open
                Call ExecuteSqlFile (scripts & "\buildtables.sql",CN,dbName,dbVer,0)
                If Err.Number <> 0 Then 
                        Wscript.Echo "Error: " & Err.Number & "; " & Err.Description
                        Wscript.Quit(7)
                End If
                CN.Close
                CN.Open
                Call ExecuteSqlFile (scripts & "\fillinitdata.sql", CN, dbName,dbVer,1)
                If Err.Number <> 0 Then
                        Wscript.Echo "Error: " & Err.Number & "; " & Err.Description
                        Wscript.Quit(8)
                End If
                CN.Close
        End If
        Call CreateSCSRegistry (dbname, sqlServer, trustedConn) 
End If  

' Create DB related registry key
Sub CreateSCSRegistry (dbname, sqlServer, trustedConn)
        WshShell.RegWrite RegistryPath & "\DB\DBName", dbName, "REG_SZ"

        If Err.Number <> 0 Then 
                Wscript.Echo "Error: " & Err.Number & "; " & Err.Description
                Wscript.Quit(3)
        End If
        WshShell.RegWrite RegistryPath & "\DB\DBPath", sqlServer, "REG_SZ"
        If Err.Number <> 0 Then 
                Wscript.Echo "Error: " & Err.Number & "; " & Err.Description
                Wscript.Quit(3)
        End If
        WshShell.RegWrite RegistryPath & "\DB\\IsLite", 0, "REG_DWORD"
        If Err.Number <> 0 Then 
                Wscript.Echo "Error: " & Err.Number & "; " & Err.Description
                Wscript.Quit(3)
        End If

        WshShell.RegWrite RegistryPath & "\DB\DB_TRUSTED_CONNECTION", trustedConn, "REG_DWORD"
End Sub

' Delete DB related registry key
Sub DeleteSCSRegistry ()
        On Error Resume Next
        WshShell.RegDelete RegistryPath & "\DB\"
        If Err.Number <> 0 Then 
                Wscript.Echo "Error: " & Err.Number & "; " & Err.Description
                Wscript.Quit(3)
        End If
End Sub


Sub ExecuteSqlFile(sFileName,sqlConnection,sDBName,sDBVersion,bNeedVer)
        On Error Resume Next
        Set Fso = CreateObject("Scripting.FileSystemObject")
        Set Fil = Fso.OpenTextFile(sFileName,ForReading,False)',TristateTrue)
        FileLine = Fil.ReadAll
        FileContents = Replace(FileLine,"$(db_name)",sDBName)
        If bNeedVer <> 0 Then
                FileContents = Replace(FileContents,"$(scs_version)",sDBVersion)
                                End If
        CN.Execute FileContents
        If Err.Number <> 0 Then 
                Wscript.Echo "Error: " & Err.Number & "; " & Err.Description
                Wscript.Quit(2)
        End If
        Fil.Close
End Sub

Function OsType()
strComputer = "."
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colOperatingSystems = objWMIService.ExecQuery _
    ("Select * from Win32_ComputerSystem")
For Each objOperatingSystem in colOperatingSystems
        osType = objOperatingSystem.SystemType
Next
os = Left(osType,InStr(1,osType,"-") - 1)

OsType = os

End Function

Function RegistryPath()
        If UCase(OsType()) = "X86" Then
                root = "HKLM\SOFTWARE"
        Else
                root = "HKLM\SOFTWARE\Wow6432Node"
        End If  
        RegistryPath = root & "\Intel\Intel(R) Setup and Configuration Software\8.1.0"
End Function


'' SIG '' Begin signature block
'' SIG '' MIIbAAYJKoZIhvcNAQcCoIIa8TCCGu0CAQExCzAJBgUr
'' SIG '' DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
'' SIG '' gjcCAR4wJAIBAQQQTvApFpkntU2P5azhDxfrqwIBAAIB
'' SIG '' AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFEaeaDYxfe+c
'' SIG '' K5zaMaS/HseB5DjqoIIW4TCCAz0wggKmoAMCAQICAwWw
'' SIG '' /zANBgkqhkiG9w0BAQUFADBOMQswCQYDVQQGEwJVUzEQ
'' SIG '' MA4GA1UEChMHRXF1aWZheDEtMCsGA1UECxMkRXF1aWZh
'' SIG '' eCBTZWN1cmUgQ2VydGlmaWNhdGUgQXV0aG9yaXR5MB4X
'' SIG '' DTA2MDIxNjE4MDEzMFoXDTE2MDIxOTE4MDEzMFowUjEL
'' SIG '' MAkGA1UEBhMCVVMxGjAYBgNVBAoTEUludGVsIENvcnBv
'' SIG '' cmF0aW9uMScwJQYDVQQDEx5JbnRlbCBFeHRlcm5hbCBC
'' SIG '' YXNpYyBQb2xpY3kgQ0EwggEiMA0GCSqGSIb3DQEBAQUA
'' SIG '' A4IBDwAwggEKAoIBAQDBpd/XOb9QVqEZ8mQ1042TdOIq
'' SIG '' 3ATDIsV2xDyt30yLyMR5Wjtus0bn3B+he89BiNO/LP6+
'' SIG '' rFzEwlD55PlX+HLGIKeNNG97dqyc30FElEUjZzTZFq2N
'' SIG '' 4e3kVJ/XAEEgANzV8v9qp7qWwxugPgfc3z9BkYot+Cif
'' SIG '' ozexHLb/hEZj+yISCU61kRZvuSQ0E11yYL4dRgcglJea
'' SIG '' Ho3oX57rvIckaLsYV5/1Aj+R8DM1Ppk965XQAKsHfnyT
'' SIG '' 7C4S50T4lVn4lz36wOdNZn/zegG1zp41lnoTFfT4KuKV
'' SIG '' JH5x7YD1p6KbgJCKLovnujGuohquBNfdXKpZkvz6pGv+
'' SIG '' iC1HawJdAgMBAAGjgaAwgZ0wDgYDVR0PAQH/BAQDAgEG
'' SIG '' MB0GA1UdDgQWBBQaxgxKxEdvqNutK/D0Vgaj7TdUDDA6
'' SIG '' BgNVHR8EMzAxMC+gLaArhilodHRwOi8vY3JsLmdlb3Ry
'' SIG '' dXN0LmNvbS9jcmxzL3NlY3VyZWNhLmNybDAfBgNVHSME
'' SIG '' GDAWgBRI5mj5K9KylddH2CMgEE8zmJCf1DAPBgNVHRMB
'' SIG '' Af8EBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBABMQOK2k
'' SIG '' VKVIlUWwLTdywJ+e2O+PC/uQltK2F3lRyrPfBn69tOkI
'' SIG '' P4SgDJOfsxyobIrPLe75kBLw+Dom13OBDp/EMZJZ1Cgl
'' SIG '' QfVV8co9mT3aZMjSGGQiMgkJLR3jMfr900fXZKj5XeqC
'' SIG '' J+JP0mEhJGEdVCY+FFlksJjV86fDrq1QMIID7jCCA1eg
'' SIG '' AwIBAgIQfpPr+3zGTlnqS5p31Ab8OzANBgkqhkiG9w0B
'' SIG '' AQUFADCBizELMAkGA1UEBhMCWkExFTATBgNVBAgTDFdl
'' SIG '' c3Rlcm4gQ2FwZTEUMBIGA1UEBxMLRHVyYmFudmlsbGUx
'' SIG '' DzANBgNVBAoTBlRoYXd0ZTEdMBsGA1UECxMUVGhhd3Rl
'' SIG '' IENlcnRpZmljYXRpb24xHzAdBgNVBAMTFlRoYXd0ZSBU
'' SIG '' aW1lc3RhbXBpbmcgQ0EwHhcNMTIxMjIxMDAwMDAwWhcN
'' SIG '' MjAxMjMwMjM1OTU5WjBeMQswCQYDVQQGEwJVUzEdMBsG
'' SIG '' A1UEChMUU3ltYW50ZWMgQ29ycG9yYXRpb24xMDAuBgNV
'' SIG '' BAMTJ1N5bWFudGVjIFRpbWUgU3RhbXBpbmcgU2Vydmlj
'' SIG '' ZXMgQ0EgLSBHMjCCASIwDQYJKoZIhvcNAQEBBQADggEP
'' SIG '' ADCCAQoCggEBALGss0lUS5ccEgrYJXmRIlcqb9y4JsRD
'' SIG '' c2vCvy5QWvsUwnaOQwElQ7Sh4kX06Ld7w3TMIte0lAAC
'' SIG '' 903tv7S3RCRrzV9FO9FEzkMScxeCi2m0K8uZHqxyGyZN
'' SIG '' cR+xMd37UWECU6aq9UksBXhFpS+JzueZ5/6M4lc/PcaS
'' SIG '' 3Er4ezPkeQr78HWIQZz/xQNRmarXbJ+TaYdlKYOFwmAU
'' SIG '' xMjJOxTawIHwHw103pIiq8r3+3R8J+b3Sht/p8OeLa6K
'' SIG '' 6qbmqicWfWH3mHERvOJQoUvlXfrlDqcsn6plINPYlujI
'' SIG '' fKVOSET/GeJEB5IL12iEgF1qeGRFzWBGflTBE3zFefHJ
'' SIG '' wXECAwEAAaOB+jCB9zAdBgNVHQ4EFgQUX5r1blzMzHSa
'' SIG '' 1N197z/b7EyALt0wMgYIKwYBBQUHAQEEJjAkMCIGCCsG
'' SIG '' AQUFBzABhhZodHRwOi8vb2NzcC50aGF3dGUuY29tMBIG
'' SIG '' A1UdEwEB/wQIMAYBAf8CAQAwPwYDVR0fBDgwNjA0oDKg
'' SIG '' MIYuaHR0cDovL2NybC50aGF3dGUuY29tL1RoYXd0ZVRp
'' SIG '' bWVzdGFtcGluZ0NBLmNybDATBgNVHSUEDDAKBggrBgEF
'' SIG '' BQcDCDAOBgNVHQ8BAf8EBAMCAQYwKAYDVR0RBCEwH6Qd
'' SIG '' MBsxGTAXBgNVBAMTEFRpbWVTdGFtcC0yMDQ4LTEwDQYJ
'' SIG '' KoZIhvcNAQEFBQADgYEAAwmbj3nvf1kwqu9otfrjCR27
'' SIG '' T4IGXTdfplKfFo3qHJIJRG71betYfDDo+WmNI3MLEm9H
'' SIG '' qa45EfgqsZuwGsOO61mWAK3ODE2y0DGmCFwqevzieh1X
'' SIG '' TKhlGOl5QGIllm7HxzdqgyEIjkHq3dlXPx13SYcqFgZe
'' SIG '' pjhqIhKjURmDfrYwggSjMIIDi6ADAgECAhAOz/Q4yP6/
'' SIG '' NW4E2GqYGxpQMA0GCSqGSIb3DQEBBQUAMF4xCzAJBgNV
'' SIG '' BAYTAlVTMR0wGwYDVQQKExRTeW1hbnRlYyBDb3Jwb3Jh
'' SIG '' dGlvbjEwMC4GA1UEAxMnU3ltYW50ZWMgVGltZSBTdGFt
'' SIG '' cGluZyBTZXJ2aWNlcyBDQSAtIEcyMB4XDTEyMTAxODAw
'' SIG '' MDAwMFoXDTIwMTIyOTIzNTk1OVowYjELMAkGA1UEBhMC
'' SIG '' VVMxHTAbBgNVBAoTFFN5bWFudGVjIENvcnBvcmF0aW9u
'' SIG '' MTQwMgYDVQQDEytTeW1hbnRlYyBUaW1lIFN0YW1waW5n
'' SIG '' IFNlcnZpY2VzIFNpZ25lciAtIEc0MIIBIjANBgkqhkiG
'' SIG '' 9w0BAQEFAAOCAQ8AMIIBCgKCAQEAomMLOUS4uyOnREm7
'' SIG '' Dv+h8GEKU5OwmNutLA9KxW7/hjxTVQ8VzgQ/K/2plpbZ
'' SIG '' vmF5C1vJTIZ25eBDSyKV7sIrQ8Gf2Gi0jkBP7oU4uRHF
'' SIG '' I/JkWPAVMm9OV6GuiKQC1yoezUvh3WPVF4kyW7BemVqo
'' SIG '' nShQDhfultthO0VRHc8SVguSR/yrrvZmPUescHLnkudf
'' SIG '' zRC5xINklBm9JYDh6NIipdC6Anqhd5NbZcPuF3S8QYYq
'' SIG '' 3AhMjJKMkS2ed0QfaNaodHfbDlsyi1aLM73ZY8hJnTrF
'' SIG '' xeozC9Lxoxv0i77Zs1eLO94Ep3oisiSuLsdwxb5OgyYI
'' SIG '' +wu9qU+ZCOEQKHKqzQIDAQABo4IBVzCCAVMwDAYDVR0T
'' SIG '' AQH/BAIwADAWBgNVHSUBAf8EDDAKBggrBgEFBQcDCDAO
'' SIG '' BgNVHQ8BAf8EBAMCB4AwcwYIKwYBBQUHAQEEZzBlMCoG
'' SIG '' CCsGAQUFBzABhh5odHRwOi8vdHMtb2NzcC53cy5zeW1h
'' SIG '' bnRlYy5jb20wNwYIKwYBBQUHMAKGK2h0dHA6Ly90cy1h
'' SIG '' aWEud3Muc3ltYW50ZWMuY29tL3Rzcy1jYS1nMi5jZXIw
'' SIG '' PAYDVR0fBDUwMzAxoC+gLYYraHR0cDovL3RzLWNybC53
'' SIG '' cy5zeW1hbnRlYy5jb20vdHNzLWNhLWcyLmNybDAoBgNV
'' SIG '' HREEITAfpB0wGzEZMBcGA1UEAxMQVGltZVN0YW1wLTIw
'' SIG '' NDgtMjAdBgNVHQ4EFgQURsZpow5KFB7VTNpSYxc/Xja8
'' SIG '' DeYwHwYDVR0jBBgwFoAUX5r1blzMzHSa1N197z/b7EyA
'' SIG '' Lt0wDQYJKoZIhvcNAQEFBQADggEBAHg7tJEqAEzwj2Iw
'' SIG '' N3ijhCcHbxiy3iXcoNSUA6qGTiWfmkADHN3O43nLIWgG
'' SIG '' 2rYytG2/9CwmYzPkSWRtDebDZw73BaQ1bHyJFsbpst+y
'' SIG '' 6d0gxnEPzZV03LZc3r03H0N45ni1zSgEIKOq8UvEiCmR
'' SIG '' DoDREfzdXHZuT14ORUZBbg2w6jiasTraCXEQ/Bx5tIB7
'' SIG '' rGn0/Zy2DBYr8X9bCT2bW+IWyhOBbQAuOA2oKY8s4bL0
'' SIG '' WqkBrxWcLC9JG9siu8P+eJRRw4axgohd8D20UaF5Mysu
'' SIG '' e7ncIAkTcetqGVvP6KUwVyyJST+5z3/Jvz4iaGNTmr1p
'' SIG '' dKzFHTx/kuDDvBzYBHUwggV1MIIEXaADAgECAgov9eV6
'' SIG '' AAEAAF/vMA0GCSqGSIb3DQEBBQUAMFYxCzAJBgNVBAYT
'' SIG '' AlVTMRowGAYDVQQKExFJbnRlbCBDb3Jwb3JhdGlvbjEr
'' SIG '' MCkGA1UEAxMiSW50ZWwgRXh0ZXJuYWwgQmFzaWMgSXNz
'' SIG '' dWluZyBDQSAzQTAeFw0xMTA0MTgxNTAxMzVaFw0xNDA0
'' SIG '' MDIxNTAxMzVaMIGQMRQwEgYDVQQKEwtJbnRlbCBDb3Jw
'' SIG '' LjEgMB4GA1UECxMXQ2xpZW50IENvbXBvbmVudHMgR3Jv
'' SIG '' dXAxNDAyBgNVBAMTK0ludGVsIENvcnBvcmF0aW9uIC0g
'' SIG '' Q2xpZW50IENvbXBvbmVudHMgR3JvdXAxIDAeBgkqhkiG
'' SIG '' 9w0BCQEWEXN1cHBvcnRAaW50ZWwuY29tMIGfMA0GCSqG
'' SIG '' SIb3DQEBAQUAA4GNADCBiQKBgQCU94fi/Xj4Tii7DNEF
'' SIG '' FxZS07I6ZF+nHPPgQhZgkMnPh7UIw4QkAc8eO8rN4y5K
'' SIG '' 133c0I0l3FWGYGULpffKIgkLATczXWnB7PavM/WprDmg
'' SIG '' b5pmFCTKzKSc+iCRPm2hzzbZ0XrFvLaCsrA8diFLoK8m
'' SIG '' 6MkW0045jeckwzWV+9kI4QIDAQABo4ICjDCCAogwCwYD
'' SIG '' VR0PBAQDAgeAMD0GCSsGAQQBgjcVBwQwMC4GJisGAQQB
'' SIG '' gjcVCIbDjHWEmeVRg/2BKIWOn1OCkcAJZ4Tbwz6HtZ4t
'' SIG '' AgFkAgEHMB0GA1UdDgQWBBS8gzCIBVJgHqHhhWnVHyRo
'' SIG '' Gy66CTAfBgNVHSMEGDAWgBSqFmavtz1WU2CuDcLt8+4H
'' SIG '' y1FgfjCBzwYDVR0fBIHHMIHEMIHBoIG+oIG7hldodHRw
'' SIG '' Oi8vd3d3LmludGVsLmNvbS9yZXBvc2l0b3J5L0NSTC9J
'' SIG '' bnRlbCUyMEV4dGVybmFsJTIwQmFzaWMlMjBJc3N1aW5n
'' SIG '' JTIwQ0ElMjAzQSgxKS5jcmyGYGh0dHA6Ly9jZXJ0aWZp
'' SIG '' Y2F0ZXMuaW50ZWwuY29tL3JlcG9zaXRvcnkvQ1JML0lu
'' SIG '' dGVsJTIwRXh0ZXJuYWwlMjBCYXNpYyUyMElzc3Vpbmcl
'' SIG '' MjBDQSUyMDNBKDEpLmNybDCB9QYIKwYBBQUHAQEEgegw
'' SIG '' geUwbAYIKwYBBQUHMAKGYGh0dHA6Ly93d3cuaW50ZWwu
'' SIG '' Y29tL3JlcG9zaXRvcnkvY2VydGlmaWNhdGVzL0ludGVs
'' SIG '' JTIwRXh0ZXJuYWwlMjBCYXNpYyUyMElzc3VpbmclMjBD
'' SIG '' QSUyMDNBKDEpLmNydDB1BggrBgEFBQcwAoZpaHR0cDov
'' SIG '' L2NlcnRpZmljYXRlcy5pbnRlbC5jb20vcmVwb3NpdG9y
'' SIG '' eS9jZXJ0aWZpY2F0ZXMvSW50ZWwlMjBFeHRlcm5hbCUy
'' SIG '' MEJhc2ljJTIwSXNzdWluZyUyMENBJTIwM0EoMSkuY3J0
'' SIG '' MBMGA1UdJQQMMAoGCCsGAQUFBwMDMBsGCSsGAQQBgjcV
'' SIG '' CgQOMAwwCgYIKwYBBQUHAwMwDQYJKoZIhvcNAQEFBQAD
'' SIG '' ggEBAD1iEzZtvZkHprhC+b4ypOe/OW/D3VFirocFaFQa
'' SIG '' D4ycpP4L8SKkBb7mIgx2hH/gOtI9dEOq8O5nY3Luoh8m
'' SIG '' Ovr0smlZk0VI67XZQlxYjlJd8uNyAQ3s8iNh6e98Iqp2
'' SIG '' AKFZFo/npiO/AKfOgBtQ5WjTtApjA4araXBr9I1zwkPO
'' SIG '' 6Nz/PF6nM7qTLkqskPJY5WSJwv2QvnNIf/nBfT0FqoBP
'' SIG '' IAbc+QvlJ+gcldSoJCWeHvdU7/TVdKkS2/d3puDDMqDK
'' SIG '' WSt4KGHJCEHMW2KoUSwi+3qwHVsW5cgXHsx/iLkg+07U
'' SIG '' cKhlkTMGcXO94mOljx1lnV7TU8T3tGOF1R2ZdOQwggWK
'' SIG '' MIIEcqADAgECAgphHoC3AAAAAAAHMA0GCSqGSIb3DQEB
'' SIG '' BQUAMFIxCzAJBgNVBAYTAlVTMRowGAYDVQQKExFJbnRl
'' SIG '' bCBDb3Jwb3JhdGlvbjEnMCUGA1UEAxMeSW50ZWwgRXh0
'' SIG '' ZXJuYWwgQmFzaWMgUG9saWN5IENBMB4XDTA5MDUxNTE5
'' SIG '' MjUxM1oXDTE1MDUxNTE5MzUxM1owVjELMAkGA1UEBhMC
'' SIG '' VVMxGjAYBgNVBAoTEUludGVsIENvcnBvcmF0aW9uMSsw
'' SIG '' KQYDVQQDEyJJbnRlbCBFeHRlcm5hbCBCYXNpYyBJc3N1
'' SIG '' aW5nIENBIDNBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8A
'' SIG '' MIIBCgKCAQEAwY+AYuc47kg7OUdF+vVd8JVv3tXgFHZB
'' SIG '' Wk9nsKtwhD4UZrYocP/p0+PDE4iu0VN19bgFiKR0WbFR
'' SIG '' lUFZFZnlXJ7ZwJ0bMVaogQ4TV7Xuc3HWMzxnoZkSpsNR
'' SIG '' L68G4+UEUcpChWbkFo0genKfZdnGrCNX2Pn9ysh8+sIa
'' SIG '' y2T7bO1SFVLw4cSDM7vzg+AP2k6AGecBtkeYQzUVhiZD
'' SIG '' qJtL6BUIfOhsNgb5hF5GIekUpYHGlS4/5fRZgXD8lrUl
'' SIG '' bs3ykQsAUPDRQUFYWCK/VAi6x0u9LLyZywsnnDLTnQ5V
'' SIG '' zFKGqhTGHD/C/K66hDG+YQ+OFFrULahIKYY3TTTcYyFB
'' SIG '' YwIDAQABo4ICXDCCAlgwDwYDVR0TAQH/BAUwAwEB/zAd
'' SIG '' BgNVHQ4EFgQUqhZmr7c9VlNgrg3C7fPuB8tRYH4wCwYD
'' SIG '' VR0PBAQDAgGGMBIGCSsGAQQBgjcVAQQFAgMBAAEwIwYJ
'' SIG '' KwYBBAGCNxUCBBYEFMIrCFPH6F0ywoKA6ZbAuCNCwBX5
'' SIG '' MBkGCSsGAQQBgjcUAgQMHgoAUwB1AGIAQwBBMB8GA1Ud
'' SIG '' IwQYMBaAFBrGDErER2+o260r8PRWBqPtN1QMMIG9BgNV
'' SIG '' HR8EgbUwgbIwga+ggayggamGTmh0dHA6Ly93d3cuaW50
'' SIG '' ZWwuY29tL3JlcG9zaXRvcnkvQ1JML0ludGVsJTIwRXh0
'' SIG '' ZXJuYWwlMjBCYXNpYyUyMFBvbGljeSUyMENBLmNybIZX
'' SIG '' aHR0cDovL2NlcnRpZmljYXRlcy5pbnRlbC5jb20vcmVw
'' SIG '' b3NpdG9yeS9DUkwvSW50ZWwlMjBFeHRlcm5hbCUyMEJh
'' SIG '' c2ljJTIwUG9saWN5JTIwQ0EuY3JsMIHjBggrBgEFBQcB
'' SIG '' AQSB1jCB0zBjBggrBgEFBQcwAoZXaHR0cDovL3d3dy5p
'' SIG '' bnRlbC5jb20vcmVwb3NpdG9yeS9jZXJ0aWZpY2F0ZXMv
'' SIG '' SW50ZWwlMjBFeHRlcm5hbCUyMEJhc2ljJTIwUG9saWN5
'' SIG '' JTIwQ0EuY3J0MGwGCCsGAQUFBzAChmBodHRwOi8vY2Vy
'' SIG '' dGlmaWNhdGVzLmludGVsLmNvbS9yZXBvc2l0b3J5L2Nl
'' SIG '' cnRpZmljYXRlcy9JbnRlbCUyMEV4dGVybmFsJTIwQmFz
'' SIG '' aWMlMjBQb2xpY3klMjBDQS5jcnQwDQYJKoZIhvcNAQEF
'' SIG '' BQADggEBAJRj/V3QxLpU9OUhw6GjVdaHX3c8PmQkMlI9
'' SIG '' 2mEsdB0zWgoD7CEx0gGhjVXNMMMr4L4TLglzKdrt+kLy
'' SIG '' 5Waf9HP+L0xm3Jzup7M+1lOf1TI5G8mZ10f47H9HJwbB
'' SIG '' 7dqCzyNR2ykQKitg6QnJmSycMiVNVS9tHvDJj6AYlirF
'' SIG '' ZeqtxUUSIyxa7z84iV/sXaABgwHDWRnnl2fiVYEgzRaq
'' SIG '' 9F5ak++Fh43tj7cwoR1IyRDtNmI1t91nkP/wp9Y0xMnh
'' SIG '' UeW06AIvWUDm3H8XhHX3bSySkvl67dKPrnRFR7es5exp
'' SIG '' Xk3InMHAHfX7XOyaV5V0UPST8XD0fFdsBATfm5w3/sIx
'' SIG '' ggOLMIIDhwIBATBkMFYxCzAJBgNVBAYTAlVTMRowGAYD
'' SIG '' VQQKExFJbnRlbCBDb3Jwb3JhdGlvbjErMCkGA1UEAxMi
'' SIG '' SW50ZWwgRXh0ZXJuYWwgQmFzaWMgSXNzdWluZyBDQSAz
'' SIG '' QQIKL/XlegABAABf7zAJBgUrDgMCGgUAoHAwEAYKKwYB
'' SIG '' BAGCNwIBDDECMAAwGQYJKoZIhvcNAQkDMQwGCisGAQQB
'' SIG '' gjcCAQQwHAYKKwYBBAGCNwIBCzEOMAwGCisGAQQBgjcC
'' SIG '' ARUwIwYJKoZIhvcNAQkEMRYEFEEI9tj8mvykh0Tqxyo7
'' SIG '' 9eOhb2wbMA0GCSqGSIb3DQEBAQUABIGABAgdeoIE8ofO
'' SIG '' RJfh63fGokMf6lO6Q/VKskJoEWR5MrMgjFM7roce9Xwq
'' SIG '' UFwXT3T4MgsoO7t6GfSPQSyi7eNMFOfz1GOPq/eeeF68
'' SIG '' IcYzggDhgwh7rJioN8q3R7lFejMMRDkrshFlckJ5Es2z
'' SIG '' Igs/5BAyp2jnPdnqYKM9ejz1xiehggILMIICBwYJKoZI
'' SIG '' hvcNAQkGMYIB+DCCAfQCAQEwcjBeMQswCQYDVQQGEwJV
'' SIG '' UzEdMBsGA1UEChMUU3ltYW50ZWMgQ29ycG9yYXRpb24x
'' SIG '' MDAuBgNVBAMTJ1N5bWFudGVjIFRpbWUgU3RhbXBpbmcg
'' SIG '' U2VydmljZXMgQ0EgLSBHMgIQDs/0OMj+vzVuBNhqmBsa
'' SIG '' UDAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqG
'' SIG '' SIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTMwMzEyMTYw
'' SIG '' OTMxWjAjBgkqhkiG9w0BCQQxFgQUCFFfDLqE7/CxGjid
'' SIG '' BVMQFLuHcRkwDQYJKoZIhvcNAQEBBQAEggEAdpDXX2Q5
'' SIG '' OS7sFO4atDwt7hKGnxqPPuWyHwSuP0ujM+APEvsHBwZG
'' SIG '' 2XKvyVPnQUKBkMprF5/ZEfK+gExPuPl89Z1t8pkygGvZ
'' SIG '' z7HN6hlvnGAG8uqHk+eRVrlNtN8kD5b/hYdSLe86tCQr
'' SIG '' MjtOp1Syeso+XycnEOAtDLsVFN1zEUqNFWA/TAYoSYgz
'' SIG '' I4S/bFdAUSmGh5Gm7RnasGsgv1oURKEu52pARSK92A1c
'' SIG '' nc6Z6/LIQwmrz4VPV2u/+NvJyorLrSU8j+Q3b5aeKVYZ
'' SIG '' z+CCBr4vctdJYse6nf0vBG/MHBBCbzmJ/wsRQY8mBUeJ
'' SIG '' MncjinQ8Rqn9aioTDE+ZIi3EQg==
'' SIG '' End signature block
