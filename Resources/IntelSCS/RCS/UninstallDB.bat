@echo off
REM ---------------------------------------------------------------------------
REM Copyright (C) Intel Corporation, 2007 - 2013. All rights reserved.
REM
REM Intel� Setup and Configuration Software (Intel� SCS) for Intel� Active Management Technology (Intel� AMT)
REM
REM The installer (IntelSCSInstaller.exe) includes options to install and uninstall
REM the database used by the RCS in database mode.
REM Alternatively, you can use UninstallDB.bat to uninstall the database.
REM This batch script is used to ensure that a VBScript is run from the 
REM cscript engine. The VBScript output is redirected to a file for logging and debugging 
REM purposes. In a production environment redirection should be either removed, 
REM or another mechanism added to prevent the log from filling the host drive.
REM
REM Syntax:
REM      uninstallDB.bat <SQL server address> <database name> [ <SQL admin user> <SQL admin password> ]
REM 
REM Example:
REM      uninstallDB.bat SQL2008.intel.com\SQL10 IntelSCS8 sa admin1!
REM 
REM Note:
REM      If the SQL admin user parameters are not supplied, Windows authentication is used.
REM      For exit codes, refer to the VBScript (DBOperationsvbs).
REM ---------------------------------------------------------------------------
cscript.exe //Nologo //I  .\DBOperations.vbs /sqlserver:%1 /dbname:%2 /sqluser:%3 /sqlpass:%4 /scripts:. /operation:DROPDB

