﻿<#
  .Synopsis 
	Makes a backup of the RCS data files using a user-independent encryption
	method.
  .Description
	The data files used by the RCS are encrypted using the Microsoft Windows
	Data Protection API (DPAPI). DPAPI uses the password of the user running
	the RCS when encrypting the files. This cmdlet lets you make backup copies
	of the RCS data files that are encrypted using a user-independent 
	encryption method. When you use the cmdlet to restore the RCS data files,
	they are re-encrypted using DPAPI.
	Note: You must use the RCS user account to run this cmdlet.
  .Parameter Operation
	Defines the operation that the cmdlet will perform. Valid values:
	  Backup  - Create backup copies of the RCS data files
	  Restore - Restore the backup copies of the RCS data files to the RCS
  .Parameter Password
	The password to use when encrypting or decrypting the RCS data files	
  .Parameter Profiles
	Backup/Restore the configuration profiles file (Profile.xml).
	Supply the name and location of the backup file.
  .Parameter PSK
	Backup/Restore the PSK keys file (PSKsStorage.dat).
	Supply the name and location of the backup file.
  .Parameter Cred
	Backup/Restore the Admin passwords file (scsadmin.dat). This file only
	exists if the admin passwords were migrated from Intel(R) SCS 5.x.
	Supply the name and location of the backup file.
  .Parameter DMP
	Backup/Restore the Digest Master Passwords file (DMP.dat). This file
	only exists if a Digest Master Password was set in the RCS.
	Supply the name and location of the backup file.
  .Parameter SkipUserVerification
	Define whether to skip or not the verification that the user running the backup is the same user as the RCS user.
  .Notes
	Copyright (c) 2010-2013, Intel Corporation. All rights reserved.
  .Example
   	RCS-Backup -Operation Backup -Password "A very long, and complicated password!" -Profiles X:\Profiles.bak -PSK X:\PSK.bak -Cred X:\Cred.bak -DMP X:\DMP.bak -SkipUserVerification $True
  .Example
	RCS-Backup -Operation Restore -Password "The password used to encrypt the files" -Profiles X:\Profiles.bak -PSK X:\PSK -Cred X:\Cred.bak -DMP X:\DMP.bak
  .Outputs
	The cmdlet does not generate output, but you can use these
	error return codes to determine the operation success status:
	0	Successful completion
	1	The RCS is not installed
	2	The RCS is running, but cannot be stopped
	3	The cmdlet was not run using the RCS user account
	    Note: If you ran the cmdlet with the correct RCS user account
		try using the -SkipUserVerification parameter with a value of $True
	4	The service is not disabled and cannot be disabled
	5	The RCS is not running, but cannot be started
	6	The RCS service is disabled, but cannot be enabled
	9	The supplied Operation was invalid (must be Backup or Restore)
	10   The configuration profiles file (Profile.xml) file does not exist
	     or cannot be read (no permissions)
	11   The PSK Keys file (PSKsStorage.dat) does not exist
	     or cannot be read (no permissions)
	12   The DMP parameter was supplied but the Digest Master Passwords file
	     (DMP.dat) file does not exist or cannot be read (no permissions)
	14   The Cred parameter was supplied but the Admin passwords file
	     (scsadmin.dat) file does not exist or cannot be read (no permissions)
	16   The backup profiles file does not exist or cannot be
	     read (no permissions)
	17   The backup PSKs file does not exist or cannot be read (no permissions)
	18   The backup Admin passwords file does not exist or cannot be
	     read (no permissions)
	19   The backup Digest Master Passwords file does not exist
	     or cannot be read (no permissions)
	20   There are no permissions to read a file
	21   Files could not be decrypted using DPAPI
	25   Files could not be encrypted using DPAPI
	26   There are no permissions to write the backup files
	30   AES Encryption or Decryption failure
	31   There are no permissions to Write SCS file
	32   There are no permissions to Read Backup file
	40   The PSK keys files in the RCS could not be read or parsed
	41   The backup PSK keys file could not be read or parsed
	42   RCS PSK file could not be Read
	99   Unknown error occurred

	To get a more detailed (verbose) output, set $DebugPreference to 'Continue'
#>
[CmdletBinding()]
Param(
	[Parameter(Mandatory=$True, Position=0, HelpMessage='Operation: backup or restore')][string]$Operation,
	[Parameter(Mandatory=$True, HelpMessage='Backup file password')][string]$Password,
	[Parameter(Mandatory=$True, HelpMessage='Profiles Backup file')][string]$Profiles,
	[Parameter(Mandatory=$True, HelpMessage='PSK Backup file')][string]$PSK,
	[Parameter(Mandatory=$False, HelpMessage='Credentials Backup file')][string]$Cred,
	[Parameter(Mandatory=$False, HelpMessage='DisgetMasterPassword Backup file')][string]$DMP,
	[Parameter(Mandatory=$False, HelpMessage='Skip User Verification')][bool]$SkipUserVerification
) 

$serviceName = 'RCSServer'
$rcsDataFolder = ([Environment]::GetFolderPath('CommonApplicationData') + '\Intel_Corporation\RCSConfServer')

# RCS data files 
$rcsProfilesFile = ($rcsDataFolder + '\Profile.xml')
$rcsPSKsFile = ($rcsDataFolder + '\PSKsStorage.dat')
$rcsDmpFile = ($rcsDataFolder + '\dmp.dat')
$rcsCredFile = ($rcsDataFolder + '\scsadmin.dat')


Function Test-ServiceStatus {
<#
  .Synopsis 
	Waits until the service reaches the desired status
  .Parameter serviceName
	Service Name to be tested
  .Parameter desiredStatus
	Desired status for the service
  .outputs
	$True if service in status
#>
	Param($serviceName, $desiredStatus)

	Write-Debug ('Waiting for ' + $serviceName + ' to be ' + $desiredStatus)

	$retrys = 5
	$sleep = 2
	for ($i = 0 ; $i -lt $retrys ; $i++ ) {
		$service = Get-Service -display $serviceName -ErrorAction SilentlyContinue
		if (!$service) 
		{
			return $False
		}
		Write-Debug ($serviceName + '''s status is ' + $service.Status)
		if ($service.Status -eq $desiredStatus) {
			return $True
		}
		Start-Sleep -Seconds $sleep
	}
	return $False
}

Function Verify-Service {
<#
  .Synopsis 
	Verify that the service exists 
#>

	trap {
		Write-Debug ($serviceName + ' could not be found.')
		exit 2
	}
	$service = Get-Service -display $serviceName -ErrorAction SilentlyContinue
	if (! $service ) {
		Write-Debug ($serviceName + ' is not installed.')
		exit 1
	}
	Write-Debug ($serviceName + '''s status is ' + $service.Status)

	$CurrentUser = [Environment]::UserDomainName + '\' + [Environment]::UserName
	$startName = Get-WmiObject -class Win32_Service -filter "Name = '$serviceName'"  -Property 'StartName'
	if (!$SkipUserVerification)
	{
		if (0 -ne [String]::Compare($startName.StartName,$CurrentUser,$True)) {
			Write-Debug ('The current user ' + $CurrentUser + ' is not Service user ' +  $startName.StartName)
			exit 3
		}
	}
}

Function StopService {
<#
  .Synopsis 
	Stops and disables the service if needed
  .Description
	Stops the service if running, and disables the service so it will not be started during backup and restore operations
  .Parameter restartService
	Boolean reference set to $True if the service was running so we can restart it once the operation finishes
  .Parameter startMode
	Reference to a string of the current Start mode of the service so we can restore it once the operation finishes
#>
	Param([ref]$restartService,
	      [ref]$startMode)

	trap {
		Write-Debug ($serviceName + ' could not be stopped.')
		exit 2
	}
	$service = Get-Service -display $serviceName -ErrorAction SilentlyContinue
	if (! $service ) {
		Write-Debug ($serviceName + ' is not installed.')
		exit 1
	}
	Write-Debug ($serviceName + '''s status is ' + $service.Status)

	# Will resstart service only if it's running
	$restartService.Value = ($service.Status -eq 'Running')
	if ($service.Status -ne 'Stopped') {
		Write-Debug ('Stopping ' + $serviceName + ' .')

		# Stop the service 
		Stop-Service $serviceName
		if (! $?) {
			Write-Debug ($serviceName + ' could not be stopped.')
			exit 2
		}
		if (! (Test-ServiceStatus $serviceName 'Stopped')) {
			Write-Debug ($serviceName + ' could not be stopped.')
			exit 2
		}
	}
	$startMode.Value = (Get-WmiObject -class Win32_Service -filter "Name = '$serviceName'"  -Property 'StartMode').StartMode
	Write-Debug ($serviceName + '''s start mode is ' + $startMode.Value)
	if ($startMode.Value -ne 'Disabled')
	{ 
		# Disable the service 
		Set-Service $serviceName -StartupType Disabled -ErrorAction SilentlyContinue
		if (! $?) {
			Write-Debug ($serviceName + ' could not be disabled.')
			exit 4
		}
		$newMode = (Get-WmiObject -class Win32_Service -filter "Name = '$serviceName'"  -Property 'StartMode').StartMode
		if ($newMode -ne 'Disabled')
		{ 
			Write-Debug ($serviceName + ' could not be disabled.')
			exit 4
		}
		Write-Debug ("$serviceName $NewMode")
	}
}

Function StartService {
<#
  .Synopsis 
	Enables and starts the service if needed
  .Description
	Enables the service and starts it
  .Parameter restartService
	If True the service is started, if not it is just enabled
  .Parameter $startMode
	StartMode of the service
#>
	Param([bool]$restartService,
		  [String]$startMode)

	trap {
		Write-Debug ($serviceName + ' could not be started.')
		exit 5
	}
	#Translate Management object names to Set-Service names
	$modes = @{ "Auto" = "Automatic"; "Manual" = "Manual"; "Disable" = "Disabled" }
	
	# Enable the service 
	$currentMode = (Get-WmiObject -class Win32_Service -filter "Name = '$serviceName'"  -Property 'StartMode').StartMode
	Write-Debug ($serviceName + '''s start mode is ' + $currentMode)
	if ($currentMode -ne $startMode) {
		Set-Service $serviceName -StartupType $modes[$startMode] -ErrorAction SilentlyContinue
		if (! $?) {
			Write-Debug ($serviceName + ' could not be enabled.')
			if ($Error.count -eq 0)
			{
				exit 6
			}
			return
		}
		$newMode = (Get-WmiObject -class Win32_Service -filter "Name = '$serviceName'"  -Property 'StartMode').StartMode
		if ($newMode -ne $startMode)
		{ 
			Write-Debug ($serviceName + ' could not be enabled.')
			if ($Error.count -eq 0)
			{
				exit 6
			}
			return
		}
		Write-Debug ("$serviceName $NewMode")
	}
	if ($restartService) {
		Write-Debug ("Restarting $serviceName")
		#start the service
		Start-Service $serviceName
		if ($DebugPreference -ne 'SilentlyContinue') {
			Test-ServiceStatus $serviceName 'Running'
		}
	}
}

Function Validate-RCSFiles {
<#
  .Synopsis 
	Checks that the parameter files are in place
  .Parameter Mode
	The operation executed
#>
	Param($Mode)
	$local:ErrorAction = 'SilentlyContinue'
	if ($Mode -eq 'backup') {
		if (-not (Test-Path $rcsProfilesFile)) {
			Write-Debug ($rcsProfilesFile + ' Does not exist (or permissions problem).')
			exit 10
		}
		
		if (-not (Test-Path $rcsPSKsFile)) {
			Write-Debug ($rcsPSKsFile + ' Does not exist (or permissions problem).')
			exit 11
		}
		
		if ($DMP -and -not (Test-Path $rcsDmpFile)) {
			Write-Debug ($rcsDmpFile + ' Does not exist (or permissions problem).')
			exit 12
		}
#		elseif ((Test-Path $rcsDmpFile) -and -not $DMP) {
#			Write-Debug ($rcsDmpFile + ' Exists but backup unspecified.')
#			exit 13
#		}
		
		if ($Cred -and -not (Test-Path $rcsCredFile)) {
			Write-Debug ($rcsCredFile + ' Does not exist (or permissions problem).')
			exit 14
		}
#		elseif ((Test-Path $rcsCredFile) -and -not $Cred) {
#			Write-Debug ($rcsCredFile + ' Exists but backup unspecified.')
#			exit 15
#		}
	}
	elseif ($Mode -eq 'restore') {
		if (-not (Test-Path $Profiles)) {
			Write-Debug ($Profiles + ' Does not exist (or permissions problem).')
			exit 16
		}
		if (-not (Test-Path $PSK)) {
			Write-Debug ($PSK + ' Does not exist (or permissions problem).')
			exit 17
		}
		if ($Cred -and -not (Test-Path $Cred)) {
			Write-Debug ($Cred + ' Does not exist (or permissions problem).')
			exit 18
		}
		if ($DMP -and -not (Test-Path $DMP)) {
			Write-Debug ($DMP + ' Does not exist (or permissions problem).')
			exit 19
		}
	}
	else {
		Write-Debug ("No Operation accepted")
		exit 9
	}
}

Function DpDecrypt-Bytes {
<#
  .Synopsis 
	Decrypts bytes using DPAPI
  .Description
	Decrypts an array of bytes using DPAPI and the credentials of the current user
  .Parameter ba
	Input bytes to be decrypted
  .Outputs
	Byte array of Decrypted input
#>
	param([byte[]]$ba)

	trap {
		Write-Debug ('DpDecrypt-Bytes: could not be decrypted. Exception:' + $_)
		throw 21
	}
	$sstr = ConvertTo-SecureString ([BitConverter]::ToString($ba) -replace ('-',''))
	$ptr  = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($sstr)
	$dec = [System.Runtime.InteropServices.Marshal]::PtrToStringUni($ptr)
	[System.Runtime.InteropServices.Marshal]::ZeroFreeBstr($ptr)
	[Text.Encoding]::Unicode.GetBytes($dec)
}

Function DpDecrypt-File {
<#
  .Synopsis 
	Decrypts a file using DPAPI
  .Description
	Decrypts a file using DPAPI and the credentials of the current user
  .Parameter ba
	File name to be decrypted
  .Outputs
	Byte array of Decrypted input
#>
	param([string]$filename)
	[byte[]]$ba = $Null
	if ($True) {
		trap {
			Write-Debug("DpDecrypt-File: $filename exception:" + $_)
			throw 20
		}
		Write-Debug ("DpDecrypt-File: $filename start")
		$ba = Get-Content $filename -encoding byte -ErrorAction SilentlyContinue
		if (! $?) {
			Write-Debug ("DpDecrypt-File: $filename could not be read.")
			throw 20
		}
	}
	DpDecrypt-Bytes $ba
}

Function DpEncrypt-String {
<#
  .Synopsis 
	Encrypts bytes using DPAPI
  .Description
	Encrypts bytes using DPAPI and the credentials of the current user
  .Parameter buff
	Input bytes to be encrypted
  .Outputs
	Byte array of Encrypted input
#>
	param([byte[]]$buff)

	trap {
		Write-Debug ('DpEncrypt-String: could not be encrypted. Exception:' + $_)
		throw 25
	}
	$sstr = ConvertTo-SecureString -string ([Text.Encoding]::Unicode.GetString($buff)) -AsPlainText -Force
	$enc= ConvertFrom-SecureString $sstr
	@($enc -split '([0-9a-fA-F]{2})' | Foreach-Object { if ($_) {[Convert]::ToByte($_, 16)}}) 
}

Function DpEncrypt-File {
<#
  .Synopsis 
	Encrypts a file using DPAPI
  .Description
	Encrypts bytes into a file using DPAPI and the credentials of the current user
  .Parameter buff
	Input bytes to be encrypted
  .Parameter filename
	File name of the encrypted result
#>
	param([byte[]]$buff,
	      [string]$filename)

	Write-Debug ("DpEncrypt-File: $filename start")
	$ba = DpEncrypt-String $buff
	if ($True) {
		trap {
			Write-Debug("DpDecrypt-File: $filename exception:" + $_)
			throw 26
		}
		Set-Content -value $ba -encoding byte $filename -ErrorAction SilentlyContinue
		if (! $?) {
			Write-Debug ("DpEncrypt-File: $filename could not be written.")
			throw 26
		}
	}
}

Function GetSha256 {
<#
  .Synopsis 
	Computes SHA-256 hash
  .Parameter password
	Input password
#>
	param([string]$password)

	$bytes = [Text.Encoding]::UTF8.GetBytes($password)
	$sha = New-Object Security.Cryptography.SHA256Managed
	$sha.ComputeHash($bytes)
}

Function AesEncDec-Bytes {
<#
  .Synopsis 
	Encrypts or Decryps  bytes using AES
  .Description
	Encrypts or decrypts an array of bytes using AES and the given password using AES-128-CBC methods 
  .Parameter ba
	Input bytes to be decrypted
  .Parameter password
	Password used to encrypt or decrypt the input
  .Parameter encrypt
	$True to encrypt, $False to decrypt
  .Outputs
	Byte array of encrypted or decrypted input
#>
	param([byte[]]$ba,
	      [string]$password,
	      [bool]$encrypt)

	trap {
		Write-Debug ('AesEncDec-Bytes: could not be en/decrypted.')
		throw 30
	}

	$algorithm = New-Object Security.Cryptography.RijndaelManaged
	$algorithm.BlockSize = 128
	$algorithm.Mode = 'CBC'
	$hash = GetSha256($password)
	$key = $hash[0..15]
	$iv = $hash[16..31]
	$ms = New-Object IO.MemoryStream
	if ($encrypt) {
		$op = $algorithm.CreateEncryptor($key, $iv)
	} else {
		$op = $algorithm.CreateDecryptor($key, $iv)
	}
	$cs = New-Object Security.Cryptography.CryptoStream $ms, $op, Write
	$cs.Write($ba, 0, $ba.length)
	$cs.FlushFinalBlock()
	$cs.Close()
	$ret = $ms.ToArray()
	$ms.Close()
	$algorithm.Clear()
	$ret
}

Function AesEncrypt-File {
<#
  .Synopsis 
	Encrypts a file using DPAPI
  .Description
	Encrypts bytes into a file using AES
  .Parameter buff
	Input bytes to be encrypted
  .Parameter password
	Password used to encrypt the input
  .Parameter filename
	File name of the encrypted result
#>
	param([byte[]]$buff,
	      [string]$filename,
	      [string]$password)

	Write-Debug ("AesEncrypt-File: $filename start")
	[byte[]]$ba = AesEncDec-Bytes $buff $password $True
	if ($True) {
		trap {
			Write-Debug ("AesEncrypt-File: $filename  exception:" + $_)
			throw 31
		}
		Set-Content -value $ba -encoding byte $filename -ErrorAction SilentlyContinue
		if (! $?) {
			Write-Debug ("AesEncrypt-File: $filename could not be written.")
			throw 31
		}
	}
}

Function AesDecrypt-File {
<#
  .Synopsis 
	Encrypts a file using AES
  .Description
	Encrypts a file using AES and the given password
  .Parameter password
	Password used to encrypt the input
  .Parameter filename
	File name of the encrypted result
  .Outputs
	Byte array of decrypted file
#>
	param([string]$filename,
	      [string]$password)
	
	[byte[]]$ba = $Null
	Write-Debug ("AesDecrypt-File: $filename start")
	if ($True) {
		trap {
			Write-Debug ("AesDecrypt-File: $filename. exception:" + $_)
			throw 32
		}
		$ba = Get-Content -path $filename -encoding byte -ErrorAction SilentlyContinue
		if (! $?) {
			Write-Debug ("AesDecrypt-File: $filename could not be read.")
			throw 32
		}
	}
	AesEncDec-Bytes $ba $password $False
}

Function ParseDecrypt-PSKs {
<#
  .Synopsis 
	Parses and Decrypts a PSK file using DPAPI
  .Description
	Parses the PSK file and decrypts the encrypted parts using DPAPI and the credentials of the current user
  .Parameter filename
	PSK file name to be decrypted
  .Outputs
	Byte array of Decrypted input. Tab is used to separate fields, and a newline to separate records.
#>
	param([string]$filename)

	trap {
		Write-Debug ("ParseDecrypt-PSKs: $filename could not be read or parsed.")
		throw 40
	}
	Write-Debug ("ParseDecrypt-PSKs: $filename start")
	return Get-Content $filename | % {
		$ppid=$_.split("`t")[0]
		$ppsbytes  = DpDecrypt-Bytes ([Convert]::FromBase64String($_.split("`t")[1]))
		$oldpbytes = DpDecrypt-Bytes ([Convert]::FromBase64String($_.split("`t")[2]))
		$newpbytes = DpDecrypt-Bytes ([Convert]::FromBase64String($_.split("`t")[3]))
		[Text.Encoding]::ASCII.GetBytes($ppid) + @([byte]9) `
		+ [Text.Encoding]::Convert([Text.Encoding]::Unicode, [Text.Encoding]::UTF8, $ppsbytes) + @([byte]9) `
		+ [Text.Encoding]::Convert([Text.Encoding]::Unicode, [Text.Encoding]::UTF8, $oldpbytes) + @([byte]9) `
		+ [Text.Encoding]::Convert([Text.Encoding]::Unicode, [Text.Encoding]::UTF8, $newpbytes) + @([byte]10)
	}
}

Function ParseEncrypt-PSKs {
<#
  .Synopsis 
	Parses and encrypts PSK bytes into the PSK file using DPAPI
  .Description
	Parses the PSK byte array and encrypts the relevant parts using DPAPI and the credentials of the current user. Write the result into a PSK file.
  .Parameter buff
	Input bytes to be encrypted. Tab is used to separate fields, and a newline to separate records.
  .Parameter filename
	PSK file name of the results
#>
		param([byte[]]$buff,
	      	[string]$filename)
	Write-Debug ("ParseEncrypt-PSKs: $filename start")
	$str = $Null
	if ($True) {
		trap {
			Write-Debug ("ParseEncrypt-PSKs: $filename could not be encrypted. exception:" + $_)
			throw 41
		}
		#convert to string in oder to parse the file.
		$str = [Text.Encoding]::UTF8.GetString($buff)
		$str = $str.split("`n") | % {
			if ($_) {
				$ppid = $_.split("`t")[0]
				$ppsbytes  = DpEncrypt-String ([Text.Encoding]::Unicode.GetBytes($_.split("`t")[1]))
				$oldpbytes = DpEncrypt-String ([Text.Encoding]::Unicode.GetBytes($_.split("`t")[2]))
				$newpbytes = DpEncrypt-String ([Text.Encoding]::Unicode.GetBytes($_.split("`t")[3]))
				$ppid + "`t" `
				+ [Convert]::ToBase64String($ppsbytes) + "`t" `
				+ [Convert]::ToBase64String($oldpbytes) + "`t" `
				+ [Convert]::ToBase64String($newpbytes)
			}
		}
	}
	if ($True) {
		trap {
			Write-Debug ("ParseEncrypt-PSKs: $filename could not be encrypted. exception:" + $_)
			throw 42
		}
		Set-Content -value $str -encoding ascii $filename -ErrorAction SilentlyContinue
		if (! $?) {
			Write-Debug ("DpEncrypt-File: $filename could not be written.")
			throw 42
		}
	}
}

#
# Main starts here
#
Write-Debug ("RCS-Backup: $Operation with $Profiles, $PSK [$Cred $DMP] ")

[Bool]$restartService = $False
[String]$startMode = ""
Verify-Service
Validate-RCSFiles $Operation.ToLower()
StopService  ([ref]$restartService)  ([ref]$startMode)
trap {
	Write-Debug ("Exception caught:" + $_)
	[int]$rc = 99
	if (![Int32]::TryParse($_.FullyQualifiedErrorId, [ref]$rc)) {
		$rc = 99
	}
	if ($startMode -ne "") {
		StartService $restartService $startMode
	}
	if ($MyInvocation.HistoryId -eq -1) #do not exit enclosing PS script
	{
		$host.SetShouldExit([int]$rc)
	}
	exit $rc
}
switch ($Operation.ToLower()) {
	'backup' {
		$ba = DpDecrypt-File $rcsProfilesFile
		AesEncrypt-File $ba $Profiles $Password
		$ba = ParseDecrypt-PSKs $rcsPSKsFile
		AesEncrypt-File $ba $PSK $password
		if ($Cred) {
			$ba = DpDecrypt-File $rcsCredFile
			AesEncrypt-File $ba $Cred $Password
		}
		if ($DMP) {
			$ba = DpDecrypt-File $rcsDmpFile
			AesEncrypt-File $ba $DMP $Password
		}
	}
	'restore' {
		$ba = AesDecrypt-File $Profiles $password
		DpEncrypt-File $ba $rcsProfilesFile
		$ba = AesDecrypt-File $PSK $password
		ParseEncrypt-PSKs $ba $rcsPSKsFile
		if ($Cred) {
			$ba = AesDecrypt-File $Cred $Password
			DpEncrypt-File $ba $rcsCredFile
		}
		if ($DMP) {
			$ba = AesDecrypt-File $DMP $Password
			DpEncrypt-File $ba $rcsDmpFile
		}
	}
	default {
		Write-Debug ("No Operation accepted")
		exit 9
	}
}
StartService $restartService $startMode
Write-Debug ("exit with 0")
exit 0


# SIG # Begin signature block
# MIIeLgYJKoZIhvcNAQcCoIIeHzCCHhsCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQU0HxKfiGBtkzLte/+iH9+iUxs
# JB+gghoFMIIDIDCCAomgAwIBAgIENd70zzANBgkqhkiG9w0BAQUFADBOMQswCQYD
# VQQGEwJVUzEQMA4GA1UEChMHRXF1aWZheDEtMCsGA1UECxMkRXF1aWZheCBTZWN1
# cmUgQ2VydGlmaWNhdGUgQXV0aG9yaXR5MB4XDTk4MDgyMjE2NDE1MVoXDTE4MDgy
# MjE2NDE1MVowTjELMAkGA1UEBhMCVVMxEDAOBgNVBAoTB0VxdWlmYXgxLTArBgNV
# BAsTJEVxdWlmYXggU2VjdXJlIENlcnRpZmljYXRlIEF1dGhvcml0eTCBnzANBgkq
# hkiG9w0BAQEFAAOBjQAwgYkCgYEAwV2xWGcIYu6gmi0fCG2RFGiYCh7+2gRvE4Ri
# IcPRfM6fBeC4AfBONOziipUEZKzxa1NfBbPLZ4C/QgKO/t0BCezhABRP/PvwDN1D
# ulsr4R+AcJkVV5MW8Q+XarfCaCMczE1ZMKxRHjuvK9buY0V7xdlfUNLjUA86iOe/
# FP3gx7kCAwEAAaOCAQkwggEFMHAGA1UdHwRpMGcwZaBjoGGkXzBdMQswCQYDVQQG
# EwJVUzEQMA4GA1UEChMHRXF1aWZheDEtMCsGA1UECxMkRXF1aWZheCBTZWN1cmUg
# Q2VydGlmaWNhdGUgQXV0aG9yaXR5MQ0wCwYDVQQDEwRDUkwxMBoGA1UdEAQTMBGB
# DzIwMTgwODIyMTY0MTUxWjALBgNVHQ8EBAMCAQYwHwYDVR0jBBgwFoAUSOZo+SvS
# spXXR9gjIBBPM5iQn9QwHQYDVR0OBBYEFEjmaPkr0rKV10fYIyAQTzOYkJ/UMAwG
# A1UdEwQFMAMBAf8wGgYJKoZIhvZ9B0EABA0wCxsFVjMuMGMDAgbAMA0GCSqGSIb3
# DQEBBQUAA4GBAFjOKer89961zgK5F7WF0bnj4JXMJTENAKaSbn+2kmOeUJXRmm/k
# Ed5jhW6Y7qj/WsjTVbJmcVfewCHrPSqnI0kBBIZCe/zuf6IWUrVnZ9NA2zsmWLIo
# dz2uFHdh1voqZiegDfqnc1zqcPGUIWVEX/r87yloqaKHee9570+sB3c4MIIDPTCC
# AqagAwIBAgIDBbD/MA0GCSqGSIb3DQEBBQUAME4xCzAJBgNVBAYTAlVTMRAwDgYD
# VQQKEwdFcXVpZmF4MS0wKwYDVQQLEyRFcXVpZmF4IFNlY3VyZSBDZXJ0aWZpY2F0
# ZSBBdXRob3JpdHkwHhcNMDYwMjE2MTgwMTMwWhcNMTYwMjE5MTgwMTMwWjBSMQsw
# CQYDVQQGEwJVUzEaMBgGA1UEChMRSW50ZWwgQ29ycG9yYXRpb24xJzAlBgNVBAMT
# HkludGVsIEV4dGVybmFsIEJhc2ljIFBvbGljeSBDQTCCASIwDQYJKoZIhvcNAQEB
# BQADggEPADCCAQoCggEBAMGl39c5v1BWoRnyZDXTjZN04ircBMMixXbEPK3fTIvI
# xHlaO26zRufcH6F7z0GI078s/r6sXMTCUPnk+Vf4csYgp400b3t2rJzfQUSURSNn
# NNkWrY3h7eRUn9cAQSAA3NXy/2qnupbDG6A+B9zfP0GRii34KJ+jN7Ectv+ERmP7
# IhIJTrWRFm+5JDQTXXJgvh1GByCUl5oejehfnuu8hyRouxhXn/UCP5HwMzU+mT3r
# ldAAqwd+fJPsLhLnRPiVWfiXPfrA501mf/N6AbXOnjWWehMV9Pgq4pUkfnHtgPWn
# opuAkIoui+e6Ma6iGq4E191cqlmS/Pqka/6ILUdrAl0CAwEAAaOBoDCBnTAOBgNV
# HQ8BAf8EBAMCAQYwHQYDVR0OBBYEFBrGDErER2+o260r8PRWBqPtN1QMMDoGA1Ud
# HwQzMDEwL6AtoCuGKWh0dHA6Ly9jcmwuZ2VvdHJ1c3QuY29tL2NybHMvc2VjdXJl
# Y2EuY3JsMB8GA1UdIwQYMBaAFEjmaPkr0rKV10fYIyAQTzOYkJ/UMA8GA1UdEwEB
# /wQFMAMBAf8wDQYJKoZIhvcNAQEFBQADgYEAExA4raRUpUiVRbAtN3LAn57Y748L
# +5CW0rYXeVHKs98Gfr206Qg/hKAMk5+zHKhsis8t7vmQEvD4OibXc4EOn8QxklnU
# KCVB9VXxyj2ZPdpkyNIYZCIyCQktHeMx+v3TR9dkqPld6oIn4k/SYSEkYR1UJj4U
# WWSwmNXzp8OurVAwggPuMIIDV6ADAgECAhB+k+v7fMZOWepLmnfUBvw7MA0GCSqG
# SIb3DQEBBQUAMIGLMQswCQYDVQQGEwJaQTEVMBMGA1UECBMMV2VzdGVybiBDYXBl
# MRQwEgYDVQQHEwtEdXJiYW52aWxsZTEPMA0GA1UEChMGVGhhd3RlMR0wGwYDVQQL
# ExRUaGF3dGUgQ2VydGlmaWNhdGlvbjEfMB0GA1UEAxMWVGhhd3RlIFRpbWVzdGFt
# cGluZyBDQTAeFw0xMjEyMjEwMDAwMDBaFw0yMDEyMzAyMzU5NTlaMF4xCzAJBgNV
# BAYTAlVTMR0wGwYDVQQKExRTeW1hbnRlYyBDb3Jwb3JhdGlvbjEwMC4GA1UEAxMn
# U3ltYW50ZWMgVGltZSBTdGFtcGluZyBTZXJ2aWNlcyBDQSAtIEcyMIIBIjANBgkq
# hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsayzSVRLlxwSCtgleZEiVypv3LgmxENz
# a8K/LlBa+xTCdo5DASVDtKHiRfTot3vDdMwi17SUAAL3Te2/tLdEJGvNX0U70UTO
# QxJzF4KLabQry5kerHIbJk1xH7Ex3ftRYQJTpqr1SSwFeEWlL4nO55nn/oziVz89
# xpLcSvh7M+R5CvvwdYhBnP/FA1GZqtdsn5Nph2Upg4XCYBTEyMk7FNrAgfAfDXTe
# kiKryvf7dHwn5vdKG3+nw54trorqpuaqJxZ9YfeYcRG84lChS+Vd+uUOpyyfqmUg
# 09iW6Mh8pU5IRP8Z4kQHkgvXaISAXWp4ZEXNYEZ+VMETfMV58cnBcQIDAQABo4H6
# MIH3MB0GA1UdDgQWBBRfmvVuXMzMdJrU3X3vP9vsTIAu3TAyBggrBgEFBQcBAQQm
# MCQwIgYIKwYBBQUHMAGGFmh0dHA6Ly9vY3NwLnRoYXd0ZS5jb20wEgYDVR0TAQH/
# BAgwBgEB/wIBADA/BgNVHR8EODA2MDSgMqAwhi5odHRwOi8vY3JsLnRoYXd0ZS5j
# b20vVGhhd3RlVGltZXN0YW1waW5nQ0EuY3JsMBMGA1UdJQQMMAoGCCsGAQUFBwMI
# MA4GA1UdDwEB/wQEAwIBBjAoBgNVHREEITAfpB0wGzEZMBcGA1UEAxMQVGltZVN0
# YW1wLTIwNDgtMTANBgkqhkiG9w0BAQUFAAOBgQADCZuPee9/WTCq72i1+uMJHbtP
# ggZdN1+mUp8WjeockglEbvVt61h8MOj5aY0jcwsSb0eprjkR+Cqxm7Aaw47rWZYA
# rc4MTbLQMaYIXCp6/OJ6HVdMqGUY6XlAYiWWbsfHN2qDIQiOQerd2Vc/HXdJhyoW
# Bl6mOGoiEqNRGYN+tjCCBKMwggOLoAMCAQICEA7P9DjI/r81bgTYapgbGlAwDQYJ
# KoZIhvcNAQEFBQAwXjELMAkGA1UEBhMCVVMxHTAbBgNVBAoTFFN5bWFudGVjIENv
# cnBvcmF0aW9uMTAwLgYDVQQDEydTeW1hbnRlYyBUaW1lIFN0YW1waW5nIFNlcnZp
# Y2VzIENBIC0gRzIwHhcNMTIxMDE4MDAwMDAwWhcNMjAxMjI5MjM1OTU5WjBiMQsw
# CQYDVQQGEwJVUzEdMBsGA1UEChMUU3ltYW50ZWMgQ29ycG9yYXRpb24xNDAyBgNV
# BAMTK1N5bWFudGVjIFRpbWUgU3RhbXBpbmcgU2VydmljZXMgU2lnbmVyIC0gRzQw
# ggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCiYws5RLi7I6dESbsO/6Hw
# YQpTk7CY260sD0rFbv+GPFNVDxXOBD8r/amWltm+YXkLW8lMhnbl4ENLIpXuwitD
# wZ/YaLSOQE/uhTi5EcUj8mRY8BUyb05Xoa6IpALXKh7NS+HdY9UXiTJbsF6ZWqid
# KFAOF+6W22E7RVEdzxJWC5JH/Kuu9mY9R6xwcueS51/NELnEg2SUGb0lgOHo0iKl
# 0LoCeqF3k1tlw+4XdLxBhircCEyMkoyRLZ53RB9o1qh0d9sOWzKLVoszvdljyEmd
# OsXF6jML0vGjG/SLvtmzV4s73gSneiKyJK4ux3DFvk6DJgj7C72pT5kI4RAocqrN
# AgMBAAGjggFXMIIBUzAMBgNVHRMBAf8EAjAAMBYGA1UdJQEB/wQMMAoGCCsGAQUF
# BwMIMA4GA1UdDwEB/wQEAwIHgDBzBggrBgEFBQcBAQRnMGUwKgYIKwYBBQUHMAGG
# Hmh0dHA6Ly90cy1vY3NwLndzLnN5bWFudGVjLmNvbTA3BggrBgEFBQcwAoYraHR0
# cDovL3RzLWFpYS53cy5zeW1hbnRlYy5jb20vdHNzLWNhLWcyLmNlcjA8BgNVHR8E
# NTAzMDGgL6AthitodHRwOi8vdHMtY3JsLndzLnN5bWFudGVjLmNvbS90c3MtY2Et
# ZzIuY3JsMCgGA1UdEQQhMB+kHTAbMRkwFwYDVQQDExBUaW1lU3RhbXAtMjA0OC0y
# MB0GA1UdDgQWBBRGxmmjDkoUHtVM2lJjFz9eNrwN5jAfBgNVHSMEGDAWgBRfmvVu
# XMzMdJrU3X3vP9vsTIAu3TANBgkqhkiG9w0BAQUFAAOCAQEAeDu0kSoATPCPYjA3
# eKOEJwdvGLLeJdyg1JQDqoZOJZ+aQAMc3c7jecshaAbatjK0bb/0LCZjM+RJZG0N
# 5sNnDvcFpDVsfIkWxumy37Lp3SDGcQ/NlXTctlzevTcfQ3jmeLXNKAQgo6rxS8SI
# KZEOgNER/N1cdm5PXg5FRkFuDbDqOJqxOtoJcRD8HHm0gHusafT9nLYMFivxf1sJ
# PZtb4hbKE4FtAC44DagpjyzhsvRaqQGvFZwsL0kb2yK7w/54lFHDhrGCiF3wPbRR
# oXkzKy57udwgCRNx62oZW8/opTBXLIlJP7nPf8m/PiJoY1OavWl0rMUdPH+S4MO8
# HNgEdTCCBXUwggRdoAMCAQICCi/15XoAAQAAX+8wDQYJKoZIhvcNAQEFBQAwVjEL
# MAkGA1UEBhMCVVMxGjAYBgNVBAoTEUludGVsIENvcnBvcmF0aW9uMSswKQYDVQQD
# EyJJbnRlbCBFeHRlcm5hbCBCYXNpYyBJc3N1aW5nIENBIDNBMB4XDTExMDQxODE1
# MDEzNVoXDTE0MDQwMjE1MDEzNVowgZAxFDASBgNVBAoTC0ludGVsIENvcnAuMSAw
# HgYDVQQLExdDbGllbnQgQ29tcG9uZW50cyBHcm91cDE0MDIGA1UEAxMrSW50ZWwg
# Q29ycG9yYXRpb24gLSBDbGllbnQgQ29tcG9uZW50cyBHcm91cDEgMB4GCSqGSIb3
# DQEJARYRc3VwcG9ydEBpbnRlbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJ
# AoGBAJT3h+L9ePhOKLsM0QUXFlLTsjpkX6cc8+BCFmCQyc+HtQjDhCQBzx47ys3j
# LkrXfdzQjSXcVYZgZQul98oiCQsBNzNdacHs9q8z9amsOaBvmmYUJMrMpJz6IJE+
# baHPNtnResW8toKysDx2IUugryboyRbTTjmN5yTDNZX72QjhAgMBAAGjggKMMIIC
# iDALBgNVHQ8EBAMCB4AwPQYJKwYBBAGCNxUHBDAwLgYmKwYBBAGCNxUIhsOMdYSZ
# 5VGD/YEohY6fU4KRwAlnhNvDPoe1ni0CAWQCAQcwHQYDVR0OBBYEFLyDMIgFUmAe
# oeGFadUfJGgbLroJMB8GA1UdIwQYMBaAFKoWZq+3PVZTYK4Nwu3z7gfLUWB+MIHP
# BgNVHR8EgccwgcQwgcGggb6ggbuGV2h0dHA6Ly93d3cuaW50ZWwuY29tL3JlcG9z
# aXRvcnkvQ1JML0ludGVsJTIwRXh0ZXJuYWwlMjBCYXNpYyUyMElzc3VpbmclMjBD
# QSUyMDNBKDEpLmNybIZgaHR0cDovL2NlcnRpZmljYXRlcy5pbnRlbC5jb20vcmVw
# b3NpdG9yeS9DUkwvSW50ZWwlMjBFeHRlcm5hbCUyMEJhc2ljJTIwSXNzdWluZyUy
# MENBJTIwM0EoMSkuY3JsMIH1BggrBgEFBQcBAQSB6DCB5TBsBggrBgEFBQcwAoZg
# aHR0cDovL3d3dy5pbnRlbC5jb20vcmVwb3NpdG9yeS9jZXJ0aWZpY2F0ZXMvSW50
# ZWwlMjBFeHRlcm5hbCUyMEJhc2ljJTIwSXNzdWluZyUyMENBJTIwM0EoMSkuY3J0
# MHUGCCsGAQUFBzAChmlodHRwOi8vY2VydGlmaWNhdGVzLmludGVsLmNvbS9yZXBv
# c2l0b3J5L2NlcnRpZmljYXRlcy9JbnRlbCUyMEV4dGVybmFsJTIwQmFzaWMlMjBJ
# c3N1aW5nJTIwQ0ElMjAzQSgxKS5jcnQwEwYDVR0lBAwwCgYIKwYBBQUHAwMwGwYJ
# KwYBBAGCNxUKBA4wDDAKBggrBgEFBQcDAzANBgkqhkiG9w0BAQUFAAOCAQEAPWIT
# Nm29mQemuEL5vjKk5785b8PdUWKuhwVoVBoPjJyk/gvxIqQFvuYiDHaEf+A60j10
# Q6rw7mdjcu6iHyY6+vSyaVmTRUjrtdlCXFiOUl3y43IBDezyI2Hp73wiqnYAoVkW
# j+emI78Ap86AG1DlaNO0CmMDhqtpcGv0jXPCQ87o3P88XqczupMuSqyQ8ljlZInC
# /ZC+c0h/+cF9PQWqgE8gBtz5C+Un6ByV1KgkJZ4e91Tv9NV0qRLb93em4MMyoMpZ
# K3goYckIQcxbYqhRLCL7erAdWxblyBcezH+IuSD7TtRwqGWRMwZxc73iY6WPHWWd
# XtNTxPe0Y4XVHZl05DCCBYowggRyoAMCAQICCmEegLcAAAAAAAcwDQYJKoZIhvcN
# AQEFBQAwUjELMAkGA1UEBhMCVVMxGjAYBgNVBAoTEUludGVsIENvcnBvcmF0aW9u
# MScwJQYDVQQDEx5JbnRlbCBFeHRlcm5hbCBCYXNpYyBQb2xpY3kgQ0EwHhcNMDkw
# NTE1MTkyNTEzWhcNMTUwNTE1MTkzNTEzWjBWMQswCQYDVQQGEwJVUzEaMBgGA1UE
# ChMRSW50ZWwgQ29ycG9yYXRpb24xKzApBgNVBAMTIkludGVsIEV4dGVybmFsIEJh
# c2ljIElzc3VpbmcgQ0EgM0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB
# AQDBj4Bi5zjuSDs5R0X69V3wlW/e1eAUdkFaT2ewq3CEPhRmtihw/+nT48MTiK7R
# U3X1uAWIpHRZsVGVQVkVmeVcntnAnRsxVqiBDhNXte5zcdYzPGehmRKmw1Evrwbj
# 5QRRykKFZuQWjSB6cp9l2casI1fY+f3KyHz6whrLZPts7VIVUvDhxIMzu/OD4A/a
# ToAZ5wG2R5hDNRWGJkOom0voFQh86Gw2BvmEXkYh6RSlgcaVLj/l9FmBcPyWtSVu
# zfKRCwBQ8NFBQVhYIr9UCLrHS70svJnLCyecMtOdDlXMUoaqFMYcP8L8rrqEMb5h
# D44UWtQtqEgphjdNNNxjIUFjAgMBAAGjggJcMIICWDAPBgNVHRMBAf8EBTADAQH/
# MB0GA1UdDgQWBBSqFmavtz1WU2CuDcLt8+4Hy1FgfjALBgNVHQ8EBAMCAYYwEgYJ
# KwYBBAGCNxUBBAUCAwEAATAjBgkrBgEEAYI3FQIEFgQUwisIU8foXTLCgoDplsC4
# I0LAFfkwGQYJKwYBBAGCNxQCBAweCgBTAHUAYgBDAEEwHwYDVR0jBBgwFoAUGsYM
# SsRHb6jbrSvw9FYGo+03VAwwgb0GA1UdHwSBtTCBsjCBr6CBrKCBqYZOaHR0cDov
# L3d3dy5pbnRlbC5jb20vcmVwb3NpdG9yeS9DUkwvSW50ZWwlMjBFeHRlcm5hbCUy
# MEJhc2ljJTIwUG9saWN5JTIwQ0EuY3JshldodHRwOi8vY2VydGlmaWNhdGVzLmlu
# dGVsLmNvbS9yZXBvc2l0b3J5L0NSTC9JbnRlbCUyMEV4dGVybmFsJTIwQmFzaWMl
# MjBQb2xpY3klMjBDQS5jcmwwgeMGCCsGAQUFBwEBBIHWMIHTMGMGCCsGAQUFBzAC
# hldodHRwOi8vd3d3LmludGVsLmNvbS9yZXBvc2l0b3J5L2NlcnRpZmljYXRlcy9J
# bnRlbCUyMEV4dGVybmFsJTIwQmFzaWMlMjBQb2xpY3klMjBDQS5jcnQwbAYIKwYB
# BQUHMAKGYGh0dHA6Ly9jZXJ0aWZpY2F0ZXMuaW50ZWwuY29tL3JlcG9zaXRvcnkv
# Y2VydGlmaWNhdGVzL0ludGVsJTIwRXh0ZXJuYWwlMjBCYXNpYyUyMFBvbGljeSUy
# MENBLmNydDANBgkqhkiG9w0BAQUFAAOCAQEAlGP9XdDEulT05SHDoaNV1odfdzw+
# ZCQyUj3aYSx0HTNaCgPsITHSAaGNVc0wwyvgvhMuCXMp2u36QvLlZp/0c/4vTGbc
# nO6nsz7WU5/VMjkbyZnXR/jsf0cnBsHt2oLPI1HbKRAqK2DpCcmZLJwyJU1VL20e
# 8MmPoBiWKsVl6q3FRRIjLFrvPziJX+xdoAGDAcNZGeeXZ+JVgSDNFqr0XlqT74WH
# je2PtzChHUjJEO02YjW33WeQ//Cn1jTEyeFR5bToAi9ZQObcfxeEdfdtLJKS+Xrt
# 0o+udEVHt6zl7GleTcicwcAd9ftc7JpXlXRQ9JPxcPR8V2wEBN+bnDf+wjGCA5Mw
# ggOPAgEBMGQwVjELMAkGA1UEBhMCVVMxGjAYBgNVBAoTEUludGVsIENvcnBvcmF0
# aW9uMSswKQYDVQQDEyJJbnRlbCBFeHRlcm5hbCBCYXNpYyBJc3N1aW5nIENBIDNB
# Agov9eV6AAEAAF/vMAkGBSsOAwIaBQCgeDAYBgorBgEEAYI3AgEMMQowCKACgACh
# AoAAMBkGCSqGSIb3DQEJAzEMBgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAM
# BgorBgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBTF0voE09M97hZWvCxVN2BCTgV2
# LDANBgkqhkiG9w0BAQEFAASBgDtJbELwIcxd0w0UTMIbeCpghZtDjNomnCQcf4Ma
# 5v7Mg46agA7jkqBqG985IgzeHYDgng11Ob85uyFGxAFqqQvnDgfmjMLGTvugdr8T
# +xNkODAmYAOjLbwUUYTftI1ihytPNfpKWcakDgGGpBfmA8ZguKITGhTK8djRs2Br
# j3NmoYICCzCCAgcGCSqGSIb3DQEJBjGCAfgwggH0AgEBMHIwXjELMAkGA1UEBhMC
# VVMxHTAbBgNVBAoTFFN5bWFudGVjIENvcnBvcmF0aW9uMTAwLgYDVQQDEydTeW1h
# bnRlYyBUaW1lIFN0YW1waW5nIFNlcnZpY2VzIENBIC0gRzICEA7P9DjI/r81bgTY
# apgbGlAwCQYFKw4DAhoFAKBdMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJ
# KoZIhvcNAQkFMQ8XDTEzMDMxMjE2MDkzMFowIwYJKoZIhvcNAQkEMRYEFANGSgN/
# FF3NHvh8aOUVms+tc8c6MA0GCSqGSIb3DQEBAQUABIIBABCrh8QoASlRntpXGwDM
# GtYTlbE14G9AbwvKWcPRjswneTi1pDjuvd/a1nKYJ/6I9FhKSjIuhEoor3Mc8M4U
# UIHe3U1bEwDulk2is6zDBSC638wXpAe6SfA+l4sdnRBJ0KE7ei9XPBPo7omIYz3f
# sXjiRfrz62hx4enCPmUw0vPP5unnZvxOmhHZKSTPFjQ0zZrNCRiVGSLlggYqKjBl
# EPZwefWN2iprn5MZC8e1+wnFTRq1OPVEK3juiEgXqTtijByHJHf/yJH5qOrFCz6M
# qS1I/zhKMB6JmLbULLu8DmRvojkr7v2ODy614+hGgxE6P60l3bJF3V/M5Czk+lww
# eyg=
# SIG # End signature block
