REM ---------------------------------------------------------------------------
REM Copyright (C) Intel Corporation, 2007 - 2013.
REM
REM 	BMH.bat
REM
REM This batch script is used to ensure that a VBScript is run from the 
REM cscript engine.
REM ---------------------------------------------------------------------------

@echo on
setlocal
set CS_AMT_CONFIGURATION_METHOD=2
set CS_AMT_ADDRESS=10.10.10.16
set CS_AMT_UUID=00000000-0001-0000-0000-000101000501
rem set CS_AMT_PID=4444-4444
call .\cONFIGamt.bat 
Echo | type .\BareMetal.Log
echo done
endlocal
pause