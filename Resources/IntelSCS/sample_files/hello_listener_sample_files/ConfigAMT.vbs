'-------------------------------------------------------------------------------------------------'
' Copyright � 2005-2013, Intel Corporation. All rights reserved.
' 
' ConfigAMT.vbs
' 
' 
' The Intel� Setup and Configuration Software (Intel� SCS) includes an option 
' to configure systems using scripts. For information about how to use scripts 
' with the Intel SCS, see the Intel SCS User Guide.
' 
' This example script reads these environment variables, supplied by the RCS component
' of the Intel SCS from the hello message, for input:
' CS_AMT_UUID                                   The UUID
' CS_AMT_ADDRESS                                The IP address
' CS_AMT_CONFIGURATION_METHOD   The configuration  mode of the device (1=PSK; 2=PKI)
' CS_AMT_PID                                    The PID of the TLS-PSK key (for PSK only)
' 
' The script then uses WMI to connect to the system and resolve the FQDN  
' for the Intel AMT device. Also, the script compares the UUID of the system with
' the input UUID to make sure they are the same.
'
' The script then uses the ConfigAMT command of the RCS API to send the configuration request
' to the RCS. For more information about this command, see the Implementation and Reference Guide.  
'
' Note:
'      1. The script does not include any logic for selecting which profile the RCS should use to
'         configure the system. Change the default profile to the correct one, or add the necessary logic.
'      2. To use this script, the operating system of the Intel AMT system must support WMI.
'
' Error status: 
'                       0 - The configuration request was sent to the RCS
'                       1 - Invalid UUID
'                       2 - Invalid ip address
'                       3 - Invalid Configuration method
'                       4 - Invalid PID
'                       5 - The UUIDs do not match
'                       5> - The RCS returned an error
' ------------------------------------------------------------------------------------------------' 

Option Explicit

Const DEFAULT_PROFILE_NAME = "Profile1"

Wscript.Echo  "==============Starting Script============ " & NOW

Dim objWMIService, objItem, colItems, strComputer, oShell, moniker
Dim inputUUID, inputIP, ConfigurationMethod, inputPID
Dim profileName, fqdn, uuid, hostname, domain, retVal, pid, ConfMethod, errorStr

Set oShell = WScript.CreateObject("WScript.Shell")
inputIP = oShell.ExpandEnvironmentStrings("%CS_AMT_ADDRESS%")
inputUUID = oShell.ExpandEnvironmentStrings("%CS_AMT_UUID%")
inputPID = oShell.ExpandEnvironmentStrings("%CS_AMT_PID%")
ConfigurationMethod = oShell.ExpandEnvironmentStrings("%CS_AMT_CONFIGURATION_METHOD%")
If inputUUID = "%CS_AMT_UUID%" Then
 Wscript.Echo "target UUID is a mandatory parameter"
 Wscript.Quit(1)
End If
If inputIP = "%CS_AMT_ADDRESS%" Then
 Wscript.Echo "target IP is a mandatory parameter"
 Wscript.Quit(2)
End If
If ConfigurationMethod = "%CS_AMT_CONFIGURATION_METHOD%" OR (ConfigurationMethod <> "1" AND ConfigurationMethod <> "2") Then
 Wscript.Echo "target Configuration Method is a mandatory parameter(1=PSK, 2=PKI)"
 Wscript.Quit(3)
End If
ConfMethod = 2
If ConfigurationMethod = "1" Then
        ConfMethod = 1
        If inputPID = "%CS_AMT_PID%" Then
         Wscript.Echo "target PID is a mandatory parameter for Configuration Method=1(PSK)"
         Wscript.Quit(4)
        else
                pid = inputPID
        End If
Else
        pid = ""
End If

Wscript.Echo "Resolve New AMT Properties for system"
Wscript.Echo "input UUID: " & inputUUID
Wscript.Echo "input IP: " & inputIP
Wscript.Echo "Configuration Method: " & ConfigurationMethod
Wscript.Echo "input PID: " & pid

''get FQDN via NSLOOKUP Reverse Lookup
'Wscript.Echo "Start DNS Lookup ----------------"
'
'strCommand="NSlookup " & inputIP
'set objexec=oshell.Exec(strCommand)
'strLine=objExec.StdOut.SkipLine 'Skip NameServer Info
'strLine=objExec.StdOut.SkipLine 'Skip NameServer Info
'strLine=objExec.StdOut.SkipLine 'Skip Blank line
'strLine=objExec.StdOut.ReadLine 'Read FQDN for inputIP
'Wscript.Echo strLine
'fqdn=Replace(strLine, "Name:    ", "") 'Strip off the header
'
'Wscript.Echo "End   DNS Lookup ----------------"

strComputer = inputIP
Wscript.Echo "Querying " & strComputer

moniker = "winmgmts:" _
   & "{" _
   & "impersonationLevel=impersonate," _
   & "authenticationLevel=PktPrivacy"_
   & "}" _
   & "!\\" _
   & strComputer _
   & "\root\cimv2"
  
Wscript.Echo "moniker=" &moniker
Set objWMIService = GetObject(moniker)

'Use Win32_ComputerSystem to get UUID
Set colItems = objWMIService.ExecQuery("Select * from Win32_ComputerSystemProduct")

Wscript.Echo "==============Target Host properties============" 

For Each objItem in colItems
 Wscript.Echo "UUID: " & objItem.UUID  
 uuid = objItem.UUID
Next

'Use Win32_ComputerSystem to get Networking information such as domain and host name
Set colItems = objWMIService.ExecQuery("Select * from Win32_ComputerSystem")

For Each objItem in colItems
 Wscript.Echo "Name: " & objItem.Name  
 Wscript.Echo "Domain: " & objItem.Domain 
 hostname = objItem.Name
 domain = objItem.Domain
 fqdn = objItem.Name & "." & objItem.Domain
Next

'Verify input UUID and target host UUID match
Wscript.Echo "==============Check UUID Match=====================" 
If inputUUID <> uuid Then
 Wscript.Echo "Target host operating system " _
        & inputIP & " does not have the same UUID as the input AMT"
 Wscript.Echo "Aborting..." 
 Wscript.Quit(5)
Else
 Wscript.Echo "Input UUID matches target host UUID"  
End If

' Having discovered all needed information, call ConfigAMT.
' profile id/name must be resolved arbitrarily or
' additional logic implemented
profileName = DEFAULT_PROFILE_NAME

 
Wscript.Echo "==============Call ConfigAMT=====================" 

moniker = "winmgmts:" _
   & "{" _
   & "impersonationLevel=impersonate," _
   & "authenticationLevel=PktPrivacy"_
   & "}" _
   & "!\\" _
   & "." _
   & "\root\intel_rcs:RCS_RemoteConfigurationService"
Wscript.Echo "moniker=" &moniker
Set objWMIService = GetObject(moniker)
retVal = objWMIService.ConfigAMT(uuid, fqdn, ConfMethod, profileName, pid, "", "", "", "", strComputer, "", "", "", "", "", "", "", errorStr)

Wscript.Echo ""
Wscript.Echo "==============Return Status============"
' Status returned from RCS
Wscript.Echo "RCS returned " & retval 

WSCript.Quit(retval)