@echo off
REM ---------------------------------------------------------------------------
REM Copyright (C) Intel Corporation, 2007 - 2013.
REM
REM 	BMH.bat
REM
REM This batch script is used to ensure that a VBScript is run from the 
REM cscript engine.
REM The VBScript output is redirected to a file for logging and debugging 
REM purposes. In a production environment redirection should be either removed, 
REM or another mechanism added to prevent the log from filling the host drive.
REM
REM Note that a full path to the script is provided to SCS, for example:
REM 	X:\aaa\bbb\ccc\script.bat
REM The path is decomposed to the directory part and to the script part, as in
REM 	X:\aaa\bbb\ccc
REM 	 - and -
REM	script.bat.
REM SCS then runs script.bat in
REM 	X:\aaa\bbb\ccc
REM ---------------------------------------------------------------------------
@echo on
echo ****Running the BareMetal script (ConfigAMT.vbs)****  >> BareMetal.Log
echo | time /t >> BareMetal.Log
echo | cscript.exe //Nologo "%~dp0ConfigAMT.vbs" >> "%~dp0BareMetal.Log"
