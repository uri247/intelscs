var TITEMS = [ 
 ["Introduction", "firstfile.htm", "1",
  ["About the Intel AMT Environment", "ab128727.htm", "11"],
  ["Configuration Methods and Intel AMT Versions", "ab136210.htm", "1",
   ["Host Based Configuration", "ab135685.htm", "11"],
   ["SMB/Manual Configuration", "ab142526.htm", "11"]
  ],
  ["Intel AMT and Security Considerations", "ab129886.htm", "1",
   ["Password Format", "ab145739.htm", "11"],
   ["File Encryption", "ab133840.htm", "11"],
   ["Digital Signing of Files", "ab162438.htm", "11"],
   ["Control Modes", "ab153865.htm", "11"],
   ["User Consent", "ab153874.htm", "11"],
   ["Recommendations for Secure Deployment", "ab158746.htm", "11"],
   ["Security After Configuration", "ab158934.htm", "11"],
   ["Access to the Intel MEBX", "ab161192.htm", "11"]
  ],
  ["Admin Permissions in the Intel AMT Device", "ab161370.htm", "1",
   ["Default Admin User (Digest)", "ab147364.htm", "11"],
   ["User Defined Admin User (Kerberos)", "ab155614.htm", "11"]
  ],
  ["Maintenance Policies for Intel AMT", "ab157868.htm", "1",
   ["Synchronizing the Clock", "ab157908.htm", "11"],
   ["Synchronizing Network Settings", "ab158207.htm", "11"],
   ["Re-issuing Certificates", "ab157940.htm", "11"],
   ["Replacing Active Directory Object Passwords", "ab157957.htm", "11"],
   ["Changing the ADOU Location", "ab157975.htm", "11"],
   ["Changing the Default Admin User Password", "ab158009.htm", "11"],
   ["Automating the Maintenance Tasks", "ab159187.htm", "11"]
  ],
  ["Support for KVM Redirection", "ab144092.htm", "11"]
 ],
 ["Prerequisites", "ac128328.htm", "1",
  ["Getting Started Checklist", "ac167664.htm", "11"],
  ["Supported Intel AMT Versions", "ac139785.htm", "11"],
  ["Supported Operating Systems", "ac131545.htm", "11"],
  ["Support for a Workgroup Environment", "ac167534.htm", "11"],
  ["Required User Permissions", "ac136564.htm", "11"]
 ],
 ["Quick Start Guide", "gettingstarted.htm", "11"],
 ["Using the Configuration Utility", "ae132784.htm", "1",
  ["Starting the Configuration Utility", "ae146608.htm", "11"],
  ["Configuring/Unconfiguring Individual Systems", "localoptionspage.htm", "11"],
  ["Configuring a System", "hbppage.htm", "1",
   ["Defining IP and FQDN for a Single System", "networksettingsforlocalform.htm", "11"],
   ["Encrypting the Profile", "encryptionpage.htm", "11"]
  ],
  ["SMB/Manual Configuration", "manualusbconfigdata.htm", "11"],
  ["Unconfiguring a System", "unconfiguredetails.htm", "11"],
  ["Using the Profile Designer", "profileediting.htm", "11"],
  ["Defining Manual Configuration (Multiple Systems)", "multiplesystemsusbform.htm", "11"]
 ],
 ["Defining Configuration Profiles", "af173204.htm", "1",
  ["About Configuration Profiles", "af191562.htm", "11"],
  ["Creating/Editing Configuration Profiles", "profilewelcomepage.htm", "1",
   ["Saving the Configuration Profile", "profilefinishpage.htm", "11"]
  ],
  ["Defining the Profile Scope", "profile3rdpartypage.htm", "11"],
  ["Defining Profile Optional Settings", "profilesubprofilespage.htm", "11"],
  ["Defining Active Directory Integration", "profileadpage.htm", "11"],
  ["Defining the Access Control List (ACL)", "profileaclpage.htm", "1",
   ["Adding a User to the ACL", "acl_detailsform.htm", "11"],
   ["Using Access Monitor", "af123028.htm", "11"]
  ],
  ["Defining Home Domains", "profiledomainspage.htm", "11"],
  ["Defining Remote Access", "profilera_page.htm", "1",
   ["Defining Management Presence Servers", "mpsform.htm", "11"],
   ["Defining Remote Access Policies", "remoteaccesspolicyform.htm", "11"]
  ],
  ["Defining Trusted Root Certificates", "trustedrootseditorform.htm", "11"],
  ["Defining Transport Layer Security (TLS)", "profiletlspage.htm", "1",
   ["Defining Advanced Mutual Authentication Settings", "tls_advanceform.htm", "11"]
  ],
  ["Defining Network Setups", "profilenetworkpage.htm", "1",
   ["Creating WiFi Setups", "wifiprofileform.htm", "11"],
   ["Creating 802.1x Setups", "_8021xprofileform.htm", "11"],
   ["Defining End-Point Access Control", "eacconfigureform.htm", "11"]
  ],
  ["Defining System Settings", "profilegeneralpage.htm", "1",
   ["Defining IP and FQDN Settings", "networksettingsform.htm", "11"]
  ]
 ],
 ["Using the Configurator", "ag127600.htm", "1",
  ["About the Configurator", "ag132212.htm", "11"],
  ["CLI Syntax", "ag151737.htm", "11"],
  ["Configurator Log Files", "ag153095.htm", "11"],
  ["CLI Global Options", "ag153968.htm", "11"],
  ["Verifying the Status of an Intel AMT System", "ag121094.htm", "11"],
  ["Discovering Systems", "ag117091.htm", "11"],
  ["Configuring Systems", "ag138653.htm", "11"],
  ["Configuring a System Using a USB Key", "ag154981.htm", "1",
   ["Power Package GUIDs", "ag129151.htm", "11"]
  ],
  ["Maintaining Configured Systems", "ag128793.htm", "11"],
  ["Unconfiguring Intel AMT Systems", "ag117118.htm", "11"],
  ["Running Scripts with the Configurator", "ag154269.htm", "1",
   ["What if a Failure Occurs?", "ag154468.htm", "11"],
   ["Script Runtime and Timeout", "ag156335.htm", "11"],
   ["Parameters Sent in Base64 Format", "ag156327.htm", "11"]
  ]
 ],
 ["Certification Authorities and Templates", "ah71572.htm", "1",
  ["Standalone or Enterprise CA", "ah60523.htm", "11"],
  ["Request Handling", "ah74594.htm", "11"],
  ["Required Permissions on the CA", "ah76011.htm", "11"],
  ["Defining Enterprise CA Templates", "ah53929.htm", "11"],
  ["Defining Common Names in the Certificate", "cneditorform.htm", "11"],
  ["Using Predefined Files Instead of a CA Request", "ah77098.htm", "11"],
  ["CRL XML Format", "ah69580.htm", "11"]
 ],
 ["Troubleshooting", "aj98279.htm", "1",
  ["Configuration Utility Error: &#147;Cannot Configure Intel AMT&#148;", "aj107427.htm", "11"],
  ["The Configuration Utility Takes a Long Time to Start", "aj107444.htm", "11"],
  ["Problems Using Configuration Utility on a Network Drive", "aj107461.htm", "11"],
  ["Remote Connection to Intel AMT Fails", "aj107504.htm", "11"],
  ["Error with XML File or Missing SCSVersion Tag", "aj107057.htm", "11"],
  ["Reconfiguration of Dedicated IP and FQDN Settings", "aj107187.htm", "11"],
  ["Disjointed Namespaces", "aj99194.htm", "11"],
  ["Kerberos Authentication Failure", "aj107764.htm", "11"],
  ["Error: &#147;Kerberos User is not Permitted to Configure..&#148;", "aj107805.htm", "11"],
  ["Error when Removing AD Integration (Error in SetKerberos)", "aj107839.htm", "11"],
  ["Failed Certificate Requests via Microsoft CA", "aj107713.htm", "11"],
  ["Delta Profile Fails to Configure WiFi Settings", "aj107887.htm", "11"],
  ["Disabling the Wireless Interface", "aj107875.htm", "11"]
 ],
 ["Legal Information", "legalinfo.htm", "11"]
];


var FITEMS = arr_flatten(TITEMS);

function arr_flatten (x) {
   var y = []; if (x == null) return y;
   for (var i=0; i<x.length; i++) {
      if (typeof(x[i]) == "object") {
         var flat = arr_flatten(x[i]);
         for (var j=0; j<flat.length; j++)
             y[y.length]=flat[j];
      } else {
         if ((i%3==0))
          y[y.length]=x[i+1];
      }
   }
   return y;
}

