#include "stdafx.h"
#include "IssInstallerExecutor.h"


std::string get_tmpfile_name( )
{
	char path_buffer[MAX_PATH+1];
	char filename_buffer[MAX_PATH+1];
	DWORD result;

	result = GetTempPathA( _countof(path_buffer)-1, path_buffer );
	if( result == 0 || result > MAX_PATH ) {
		throw Win32Exception( "createTmpFile" );
	}

	result = GetTempFileNameA( path_buffer, "sia", 0, filename_buffer );
	if( result == 0 ) {
		throw Win32Exception( "createTmpFile" );
	}

	return std::string(filename_buffer);
}




void CIssExecutor::setParams( IssExecutorParam const& params )
{
	// this is a structure copy by design.
	m_params = params; 
}

void CIssExecutor::createFile( )
{
	m_fname = get_tmpfile_name();
	std::ofstream tmp_file;

	tmp_file.open( m_fname );

	tmp_file <<
        "[InstallShield Silent]" << std::endl <<
        "Version=v7.00" << std::endl <<
        "File=Response File" << std::endl <<
        "[File Transfer]" << std::endl <<
        "OverwrittenReadOnly=NoToAll" << std::endl <<
        "[{78A13CBB-40C7-4E11-9405-38C5C3A98AB5}-DlgOrder]" << std::endl <<
        "Dlg0={78A13CBB-40C7-4E11-9405-38C5C3A98AB5}-Welcome-0" << std::endl <<
        "Count=6" << std::endl <<
        "Dlg1={78A13CBB-40C7-4E11-9405-38C5C3A98AB5}-UserNeedDB-0" << std::endl <<
        "Dlg2={78A13CBB-40C7-4E11-9405-38C5C3A98AB5}-SdLicense2Rtf-0" << std::endl <<
        "Dlg3={78A13CBB-40C7-4E11-9405-38C5C3A98AB5}-Service Logon-0" << std::endl <<
        "Dlg4={78A13CBB-40C7-4E11-9405-38C5C3A98AB5}-Confirm-0" << std::endl <<
        "Dlg5={78A13CBB-40C7-4E11-9405-38C5C3A98AB5}-SdFinish-0" << std::endl <<
        "[{78A13CBB-40C7-4E11-9405-38C5C3A98AB5}-Welcome-0]" << std::endl <<
        "Database=0" << std::endl <<
        "Service=1" << std::endl <<
        "Console=1" << std::endl <<
        "[{78A13CBB-40C7-4E11-9405-38C5C3A98AB5}-UserNeedDB-0]" << std::endl <<
        "UseExistingDB=0" << std::endl <<
        "[{78A13CBB-40C7-4E11-9405-38C5C3A98AB5}-SdLicense2Rtf-0]" << std::endl <<
        "Result=1" << std::endl <<
        "[{78A13CBB-40C7-4E11-9405-38C5C3A98AB5}-Service Logon-0]" << std::endl <<
		"User Name=" << m_params.serviceAccountDomain << "\\" << m_params.serviceAccountName << std::endl <<
        "Password=makeFile12" << std::endl <<
        "[{78A13CBB-40C7-4E11-9405-38C5C3A98AB5}-Confirm-0]" << std::endl <<
        "Target=C:\\Program Files\\Intel\\SCS8.2\\" << std::endl <<
        "[Application]" << std::endl <<
        "Name=Intel(R) Setup and Configuration Software" << std::endl <<
        "Version=8.2.0" << std::endl <<
        "Company=Intel" << std::endl <<
        "Lang=0009" << std::endl <<
        "[{78A13CBB-40C7-4E11-9405-38C5C3A98AB5}-SdFinish-0]" << std::endl <<
        "Result=1" << std::endl <<
        "bOpt1=0" << std::endl <<
        "bOpt2=0" << std::endl;

	tmp_file.close();
}

void CIssExecutor::wait_thread()
{
	DWORD result;
	long code;
	result = WaitForSingleObject( m_hIssProcess, INFINITE );
	if( result == WAIT_OBJECT_0 ) {
		code = 0;
	}
	else {
		code = GetLastError();
	}
	m_callback( code );
}

void CIssExecutor::execute( std::function<void(long)> callback )
{
	DWORD dwresult;
	BOOL result;
	char current_dir[MAX_PATH+1];
	char args[2000];

	m_callback = callback;

	dwresult = GetCurrentDirectoryA( _countof(current_dir)-1, current_dir );
	if( !dwresult ) {
		throw Win32Exception( "GetCurrentDirectoryA in execute" );
	}

	sprintf_s( args, "/s /f1%s /f2%s\\%s", m_fname.c_str(), current_dir, "rcs.log" );

	SHELLEXECUTEINFOA si;
	memset( &si, 0, sizeof(si) );
	si.cbSize = sizeof(si);
	si.fMask = SEE_MASK_NOCLOSEPROCESS;
	si.hwnd = NULL;
	si.lpVerb = "open";
	si.lpFile = m_params.installerLocation.c_str();
	si.lpParameters = args;
	si.lpDirectory = 0;
	si.nShow = SW_SHOW;
	si.hInstApp = 0;
	result = ShellExecuteExA(&si);

	if( !result ) {
		throw Win32Exception( "Creating the Intel Scs process" );
	}

	m_hIssProcess = si.hProcess;
	std::thread th( &CIssExecutor::wait_thread, this );
	th.detach();


}


void CIssExecutor::wait( )
{
	DWORD result;
	result = WaitForSingleObject( m_hIssProcess, INFINITE );
	if( result != WAIT_OBJECT_0 ) {
		throw Win32Exception("Waiting for Scs Installer to terminate");
	}
}

