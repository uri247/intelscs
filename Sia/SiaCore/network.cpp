/* Copyright (c) 2013 Ness Technologies
 *
 * Module:		network.cpp
 *
 * Description: A collection of user and network functions
 *
 * Author:      Uri London
 *
 */
#include "stdafx.h"
#include "errors.h"

std::wstring getFqdn( )
{
	BOOL result;
	wchar_t buffer[400];
	DWORD size = _countof(buffer) - 1;

	result = GetComputerNameEx( ComputerNameDnsFullyQualified, buffer, &size );
	if( !result ) {
		throw Win32Exception( "Trying to get computer name" );
	}

	return std::wstring(buffer);
}

std::wstring getCurrentUser( )
{
	BOOL result;
	wchar_t buffer[400];
	ULONG size = _countof(buffer) - 1;

	result = GetUserNameEx( NameSamCompatible, buffer, &size );
	if( !result ) {
		throw Win32Exception( "Get Current User" );
	}

	return std::wstring(buffer);
}