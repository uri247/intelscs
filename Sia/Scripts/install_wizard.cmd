@echo off

:run
    regsvr32 /s /n /i:user SiaCore.dll


:test
    if errorlevel 1 goto error
    echo Installation of the 'Scs Installation Wizard' succeeded
    goto end


:error
    echo error installing the Scs Installation Wizard
    goto end


:end


