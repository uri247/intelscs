#include "stdafx.h"
#include "TridentHost.h"
#include "../SiaCore/SiaCore_i.h"

CMainModule _AtlModule;

wchar_t const* regpathFeatureControl = L"Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION";
wchar_t const* regvalEmulation = L"ScsIA.exe";

HRESULT CMainModule::setIeEmulation()
{
	LONG result;
	CRegKey key;

	result = key.Open( HKEY_CURRENT_USER, regpathFeatureControl, KEY_ALL_ACCESS );
	if( result == ERROR_SUCCESS ) {
	    result = key.SetDWORDValue( regvalEmulation, 10000 );
	}

	return HRESULT_FROM_WIN32( result );
}



typedef HRESULT (__stdcall *DllGetClassObjectFunction)(REFCLSID rclsid, REFIID riid, void** ppv );


HRESULT CMainModule::regClass()
{
	HRESULT hr = S_OK;
	HMODULE hmodule = NULL;
	DllGetClassObjectFunction fn = NULL;
	CComPtr<IClassFactory> ifFactory;

	if( SUCCEEDED(hr) ) {
		hmodule = LoadLibrary( L"SiaCore.dll" );
		if( !hmodule ) {
			hr = HRESULT_FROM_WIN32(GetLastError());
		}
	}

	if( SUCCEEDED(hr) ) {
		fn = (DllGetClassObjectFunction)GetProcAddress( hmodule, "DllGetClassObject" );
		if( !fn ) {
			hr = HRESULT_FROM_WIN32(GetLastError());
		}
	}

	if( SUCCEEDED(hr) ) {
		hr = fn( __uuidof(ScsInstallationAdvisor), IID_IClassFactory, (void**)&ifFactory );
	}


	if( SUCCEEDED(hr) ) {
		hr = CoRegisterClassObject( __uuidof(ScsInstallationAdvisor), ifFactory, CLSCTX_INPROC_SERVER, REGCLS_MULTIPLEUSE, &m_dwRegister );
	}

	return hr;
}




HRESULT CMainModule::PreMessageLoop( int nCmdShow )
{
	HRESULT hr = S_OK;

	if( SUCCEEDED(hr) ) {
	    hr = CAtlExeModuleT<CMainModule>::PreMessageLoop( nCmdShow );
	}

	if( SUCCEEDED(hr) ) {
		hr = setIeEmulation();
	}

	if( SUCCEEDED(hr) ) {
		hr = regClass();
	}

	if( SUCCEEDED(hr) )
	{
		AtlAxWinInit();
		RECT rect = { 200, 200, 920, 831 };
		m_wndMain.Create(NULL, rect, L"Trident Host", WS_OVERLAPPED | WS_SYSMENU | WS_THICKFRAME, 0 );
		if( m_wndMain.m_hWnd == NULL ) {
			hr = HRESULT_FROM_WIN32( GetLastError() );
		}
	}

	if( SUCCEEDED(hr) ) {
	    m_wndMain.ShowWindow(nCmdShow);
	}

	return hr;
}


HRESULT CMainModule::PostMessageLoop()
{
	if( m_dwRegister ) {
		CoRevokeClassObject( m_dwRegister );
		m_dwRegister = 0;
	}

	AtlAxWinTerm();
	return CAtlExeModuleT<CMainModule>::PostMessageLoop();
}



int APIENTRY wWinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPTSTR    lpCmdLine,
    int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	return _AtlModule.WinMain(nCmdShow);
}

